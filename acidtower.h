#ifndef ACIDTOWER_H
#define ACIDTOWER_H

enum acidattackmode {anearest , aeldest , aweakest , astrongest };
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
#include "tile.h"
#include "QProcess"
#include "menu.h"
#include "QWheelEvent"
#include "guibutton.h"
class acidTower : public TemplateItem
{
        Q_OBJECT
public:

    acidTower(QPointF);
    acidTower(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);

    typesubItem getSubType(){return acidtower;}
    typemainItem getMainType(){return tower;}

     int getHealth(){}
     void setHealth(int){}
     long int getID();
     QPointF gettarget(){}
     void mousePressEvent(QGraphicsSceneMouseEvent *event);


    void setAttackmode(acidattackmode mode);
    ~acidTower();
     void setImage(QImage *newimage,int rank);
     virtual void setModifier(typemodifier newmodifier){}
     int getWorth(){}

     static QString getLabel(){return QString("Buy Acid");}
     static gmmode getButtonAction(){return buyacidtower ;}
     static int getInitialCost(){return 750;}
private:

    Tile *arrTiles[16][10];
    QVector<Tile *> TilesInRange ;
    int iRange ;
    acidattackmode Mode ;
    unsigned int shotsFired;

    qreal stepleftover;
    int count ;

    QPointF NearestCreep(QPointF point);
    TemplateItem* NearestCreep();
    TemplateItem* EldestCreep();
    TemplateItem* WeakestCreep();
    TemplateItem* StrongestCreep();
    QProcess process;

    QImage *image[4];
    Menu *menu;
    bool menuExist ;

     //    long int ID;
     //    QVector<TemplateItem*>  items ;
     //void addNewItem(QPointF point);
public slots:
    void menuresult(int);

};

#endif // ACIDTOWER_H
