#include "mygraphicsview.h"
#include "QMouseEvent"
#include "QGraphicsView"
#include "creep.h"
#include "tower.h"
#include "towerammo.h"
#include "tower2.h"
#include "towerammo2.h"
#include "QDebug"
#include "QGraphicsItem"

MyGraphicsView::MyGraphicsView(QGraphicsScene *scene, QWidget *parent) : QGraphicsView(scene,parent)
{
}
MyGraphicsView::MyGraphicsView(QGraphicsScene *scene, QVector<TemplateItem *> *item  , QWidget *parent, Tile *arrtiles[16][10]) : QGraphicsView(scene,parent)
{
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
            //            qDebug() << arrTiles[i][j];
        }

    items = item;
    //setEnabled(true) ;
    Mode = selltower ;
//    setMouseTracking(true);
}
void MyGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
     QGraphicsView::mouseMoveEvent(event);
}

void MyGraphicsView::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);

}

void MyGraphicsView::modeChange(gmmode mode)
{
}
