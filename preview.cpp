#include "preview.h"
#include "QGraphicsObject"
#include "QtGui"
Preview::Preview()
{
    setEnabled(false) ;
    setPos(QPoint(11,11));
    TItem = lvl1creep ;
}

QRectF Preview::boundingRect() const
{
    qreal penWidth = 1;
    int r = 316 *2 ;
    return QRectF(-0.5*r - penWidth / 2, -0.5*r - penWidth / 2,r + penWidth, r + penWidth);
}
void Preview::setmode(typesubItem titem)
{
    TItem = titem ;
    update();
}

void Preview::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
//    int a = 17*Health ; // 17 = 255/15
//    if (a > 255)
//        a = 255 ;
    painter->setPen(QColor::fromRgb(0,0,0,100));
//    painter->drawRect(QRectF(-15,-15,30,30));
//    QString Text ;
    /*
    QFont f = painter->font();
    f.setPointSize(6);
    painter->setFont(f);

    Text = QString("%1,%2").arg(pos().x()/63).arg(pos().y()/63);
    painter->drawText(QRectF(-12,-12,24,24), Qt::TextWordWrap | Qt::AlignCenter ,Text);
    //*/
    if (TItem == homingtower)
    {
//            painter->drawEllipse(-10, -10, 20, 20);
//            painter->drawArc(-5,-5,10,10,90*16,360*16);
        int r ;
        r = 225 *2;
        painter->drawEllipse(-0.5*r, -0.5*r, r, r);
        QImage image(":/graphics/tower1-preview.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),image);
//            //radius = 316
    }
    if (TItem == coldtower)
    {
//            painter->drawEllipse(-10, -10, 20, 20);
//            painter->drawArc(-5,-5,10,10,90*16,360*16);
        int r ;
        r = 225 *2;
        painter->drawEllipse(-0.5*r, -0.5*r, r, r);
        QImage image(":/graphics/tower1-preview.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),image);
//            //radius = 316
    }
    if (TItem == acidtower)
    {
//            painter->drawEllipse(-10, -10, 20, 20);
//            painter->drawArc(-5,-5,10,10,90*16,360*16);
        int r ;
        r = 225 *2;
        painter->drawEllipse(-0.5*r, -0.5*r, r, r);
        QImage image(":/graphics/tower1-preview.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),image);
//            //radius = 316
    }

        if (TItem == tower2)
        {
            int r ;
            r = 152 *2;
            painter->drawEllipse(-0.5*r, -0.5*r, r, r);

            //100
//            painter->drawArc(-5,-5,10,10,90*16,360*16);
            int radius = 10 ;
            double  ratio = 0.7 ;
            int points= 6 ;
            qreal circ = 2 * 3.14159 ;
            qreal increment = circ*1.0/points ;
            qreal offset = increment * 0.5 ;
            QPointF pointss[12];

            QImage image(":/graphics/Tower2-new-preview.png");
            painter->drawImage(QRectF(-15, -15, 30, 30),image);
            for (int i = 0 ;i <  6; i++)
            {
                pointss[2*i] = QPointF(radius*qCos(increment*i),radius*qSin(increment*i));
                pointss[2*i+1] = QPointF(ratio*radius*qCos(increment*i + offset ),ratio*radius*qSin(increment*i + offset));
            }
//            painter->drawPolygon(pointss,12);


        }
}
