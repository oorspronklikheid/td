#include "creep.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"
#include "QVector"
#include "tile.h"
Creep::Creep( QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    Health = 100 ;
    AssignId();


}
Creep::Creep(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10] ): TemplateItem( qpoint,newlayout,item,arrtiles)
{
    modifier = nomodifier ;
    lastx = layout->getTileNumber(pos()).x()  ;
    lasty = layout->getTileNumber(pos()).y() ;
//    qDebug() << "this is " << this;
//    qDebug() << "lastx" << lastx << "lasty" << lasty;


    setEnabled(false) ;
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
            qreal dx ,dy ,r ;
        }
    setPos(qpoint);
    arrTiles[lastx][lasty]->Subscribed(this);
    Health = 350 ;
    items = item ;
    AssignId();
    for(int i= items->size()-1 ; i >= 0 ; i--)
    {
        if(items->value(i)->getSubType() == creepwaypoint)
        {
            target = items->value(i)->pos();
//            if(dynamic_cast<CreepWayPoint*>(item->value(i)))
            tTarget = items->value(i);
//            qDebug() << "tTarget" << tTarget;
        }

    }
}
int Creep::getWorth()
{
    if(qrand()%20000 ==0 )
    {
        qDebug() << "You should play the lotto";
        return 1000000    ;
    }

    return qrand()%21 + qrand()%21 ;
}

void Creep::setModifier(typemodifier newmodifier)
{
    if(newmodifier ==cold){
    modifier = newmodifier ;
    modifiertime = 300 ;
    }
    if(newmodifier ==acid){
        modifier = newmodifier ;
        modifiertime = 40 ;
        }
}
void Creep::setImage(QImage *newimage,int rank)
{
    image = newimage;
//    qDebug() << "who disturbs my slumber";
}

long int Creep::getID()
{
    return ID;
}

QRectF Creep::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-15 - penWidth / 2, -15 - penWidth / 2,
                  30 + penWidth, 30 + penWidth);

}

void Creep::setHealth(int health)
{
    Health = health ;
    if(Health <= 0)
        arrTiles[lastx][lasty]->unSubscribed(this);

}

int Creep::getHealth()
{
    return Health ;
}

void Creep::settarget(QPointF)
{

}

void Creep::Progress(qreal step)
{
    //use this function to update the creep

    if(modifiertime>0)
    {
        modifiertime = modifiertime - step / 50.0 ;
    }
    else
    {
        modifier = nomodifier;
    }

    bool didfind=false;
    if(tTarget->pos() ==pos())
    {
//        qDebug() << "I have reached the target and need a new one" ;
//        qDebug() << j ;
        int d = -1 ;

        for(int i=0 ; i < items->size();i++)
        {
            if(tTarget == items->value(i))
            {
//                qDebug() << "Found my destination :P";
                d = i ;
            }

        }
//        qDebug() << "looking for next waypoint";
        didfind=false;
        for(int i = items->size()-1 ; i>d  ;i--) //we want the next waypoint
        {
//            qDebug() << "pew" << i;
            if(items->value(i)->getSubType() == creepwaypoint)
            {

                tTarget = items->value(i);
                didfind = true;
            }
        }

    }
    if(didfind==false && tTarget->pos() ==pos())
    {
        emit finished(this);
//        qDebug() << "emit signal to remove this guy and remove a live";
    }

    if (lastx != layout->getTileNumber(pos()).x() || lasty != layout->getTileNumber(pos()).y())
    {
        arrTiles[lastx][lasty]->unSubscribed(this);
        lastx = layout->getTileNumber(pos()).x() ;
        lasty = layout->getTileNumber(pos()).y() ;
        arrTiles[lastx][lasty]->Subscribed(this);
        //qDebug() << "I'm at: x=" << layout->getTileNumber(pos()).x() << " y=" << layout->getTileNumber(pos()).y() << "<Creep" ;

    }

    qreal dx , dy ;
    qreal max = 2*(step/50.0) ;
    if(modifier == cold)
        max = (max ) -  (4.0*max/5)*(modifiertime/300); // modifiertime == 100 => max = max/3  modifiertime == 0 => max = max
    if(modifier == acid)
    {
        max = 1.5*max ;
        if(Health > 5)
        Health=Health -3;
    }
    //    dx = target.x() - pos().x() ;
    //    dy = target.y() - pos().y() ;
    dx = tTarget->pos().x() - pos().x() ;
    dy = tTarget->pos().y() - pos().y() ;

    if (dx > max) dx = max ;
    if (dy > max) dy = max ;
    if (dx < -max) dx = -max ;
    if (dy < -max) dy = -max ;

    setPos(pos().x() + dx,pos().y() +dy);
    // setPos(pos().x()+4,pos().y() + 4);

//    update();

}

void Creep::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    brush.setColor(Qt::green);


//    painter->setBrush(QColor(196,196,196));
//    painter->drawRoundedRect(-10, -10, 20, 20, 5, 5);

    painter->drawImage(QRectF(-15, -15, 30, 30),*image);
    qreal red , green ;
    green = 255 *Health / 350 ;
    red = 255 - green ;
    painter->setBrush(QColor(red,green,0));
    painter->setPen(QColor(0,0,0));
    painter->drawRoundedRect(-10,12,20*Health / 350,5,3,3);
    //    painter->drawText(QRectF(-10,-12,20,10),"Creep");


}
Creep::~Creep()
{
//    qDebug() << "smells like something died";
}
