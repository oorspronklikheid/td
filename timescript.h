#ifndef TIMESCRIPT_H
#define TIMESCRIPT_H
#include "QObject"
#include "QVector"
#include "QPointF"
#include "templateitem.h"
#include "timescript.h"
#include "layoutwidget.h"
enum eImagetype { imgTower1Nearest , imgTower1Weakest , imgTower1Strongets , imgTower1Eldest , imgTower2 , imgTower1Ammo
                , imgTower2Ammo , imgCreepBeginpoint , imgCreepWaypoint , imgCreepEndpoint , imgCreep , imgTilePath, imgTilenoPath };

class TimeScript : public QObject
{
    Q_OBJECT
public:
    TimeScript(QVector<TemplateItem *> *varitem , Layoutwidget *newlayout, Tile *arrtiles[16][10]);
    int GetLevelCount();
    int ReadlevelData(QString path);
    int Readlevel(int Ilevel);

    int Progress(qreal step=50);
    void start(int levelnum);
    Layoutwidget *layout;
private:
    int ReadAddLine(QString line);
    int ReadActionSpawn(int iTime , QString line);
    int ReadActionModify(int iTime , QString line);
    int ReadActionDelete(int iTime , QString line);
    int ReadResource(int iTime , QString line);

    int VectScriptAdd(unsigned long int delay, typesubItem item , QPointF point );
    int VectScriptRemove(int i);
    int sortVectScript();
    QVector<unsigned long int >  VectScriptDelay;
    QVector<typesubItem>  VectScriptTypeitem;
    QVector<QPointF>  VectScriptpoint;

    int levelDataAdd(int number , QString filepath ,  QString description);
    int levelDataRemove(int i);
    QVector<int>      LevelDataLevelNumber;
    QVector<QString>  LevelDataLevelFilePath;
    QVector<QString>  LevelDataLevelDescription;

    Tile *arrTiles[16][10];
    QVector<TemplateItem*> *items ;

     qreal timeStamp;
     qreal tickpart;


signals:
    void spawn(TemplateItem* item);
    void deleteItem(TemplateItem* item);
    void setBackground(QImage *image);
//    void setImaget2(QImage *image,int rank);
    void setImage(QImage *image, eImagetype type);
    void nextEvent(int);
};

#endif // TIMESCRIPT_H
