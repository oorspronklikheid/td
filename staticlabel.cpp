#include "staticlabel.h"
#include "QDebug"
#include "QGraphicsObject"
#include "QtGui"

StaticLabel::StaticLabel(QPointF qpoint, QString text)
{
    Text = text ;
    setPos(qpoint);
    update();
//    qDebug() << pos();
    iSizeX =1 ;
    iSizeY =1 ;

}
void  StaticLabel::SetSize(int x,int y)
{
    iSizeX =x ;
    iSizeY =y ;

}
void StaticLabel::slotSetText(QString text)
{
    setText( text);
}
QRectF StaticLabel::boundingRect() const
{

    qreal penWidth = 3;
    int height=30 * iSizeY;
    int width=50* iSizeX ;

    QRect rect(-1 * 0.5 * width - penWidth / 2 ,-1 * 0.5 * height - penWidth / 2 , width, height);
    return rect ;
//    return QRectF(-16 - penWidth / 2, -16 - penWidth / 2, 32 + penWidth, 32 + penWidth);

}

void StaticLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    int height=30 *iSizeY ,width=50 * iSizeX ;
    QRect rect(-1 * 0.5 * width,-1 * 0.5 * height , width, height);

    float factor = 1.0*rect.width() / painter->fontMetrics().width(Text) * 1.4;
    if(iSizeY == 2)
        factor = 1.0*rect.width() / painter->fontMetrics().width(Text) *1.8;
    if(factor > 0)
    if ((factor < 1) || (factor > 1.25))
    {
        QFont f = painter->font();
        f.setPointSizeF(f.pointSizeF()*factor);
        painter->setFont(f);
    }

    QPen pen ;
    pen.setWidth(2);

    painter->setPen(pen);
    painter->drawRoundedRect(rect, 5, 5);
    painter->drawText(rect, Qt::TextWordWrap | Qt::AlignCenter,Text);



}

void StaticLabel::setText(QString text)
{
Text = text ;
update();
}
