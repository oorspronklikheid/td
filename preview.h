#ifndef PREVIEW_H
#define PREVIEW_H

#include "QRectF"
#include "QGraphicsObject"
#include "QString"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"
class Preview : public  QGraphicsObject
{
    Q_OBJECT

public:
    Preview();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void setmode(typesubItem titem);
private:
    typesubItem TItem;

};

#endif // PREVIEW_H
