#include "menu.h"
#include "QGraphicsItem"
#include "QDebug"

Menu::Menu(QGraphicsScene *scene, QPointF pos)
{
    Pos = pos ;
    //scene = new QGraphicsScene(QRect(0 , 0, 900, 650 ),this)
    refscene = scene ;
    for(int i = 0 ; i < 4 ; i++)
    {
//        menuitems.append(new MenuItem(QPoint(pos.x() +65 ,pos.y() + i*20 )  ,"one "));
//    menuitems.append(new MenuItem(QPoint(pos.x() +65 ,pos.y() + 20) ,"two "));
//    menuitems.append(new MenuItem(QPoint(pos.x() +65 ,pos.y() + 40) ,"three "));
//    menuitems.append(new MenuItem(QPoint(pos.x() +65 ,pos.y() + 60) ,"four "));
    }
    for(int i = 0 ; i < menuitems.size() ; i++)
    {
    menuitems[i]->setiOrder(i);
    connect(menuitems[i],SIGNAL(Clicked(int)),this,SLOT(itemClicked(int)));
    }

}

void Menu::addLabel(QString newlabel  )
{
    menuitems.append(new MenuItem(QPoint(Pos.x()  ,Pos.y() + (menuitems.size())* 20 )  ,newlabel));
    menuitems[menuitems.size()-1]->setiOrder(menuitems.size()-1);
    connect(menuitems[menuitems.size()-1],SIGNAL(Clicked(int)),this,SLOT(itemClicked(int)));
}

void Menu::setLabel(QString newlabel , int menuorder)
{
    if(menuitems.size() > menuorder && menuorder >= 0)
    {
        menuitems[menuorder]->setText(newlabel);
    }
    else
    {
        qDebug() << "Selected item does not exist";
    }
}

void Menu::itemClicked(int order)
{
//    qDebug() << "void Menu::itemClicked(int order)" << order ;
    emit menuitemClicked( order );
}

void Menu::dostuff()
{
    for(int i = 0 ; i < menuitems.size() ; i++)
    {
    refscene->addItem(menuitems[i]);
    }
}

Menu::~Menu()
{
    MenuItem *temp;
    for(int i = menuitems.size() -1 ; i >= 0 ; i--)
    {
        temp = menuitems[i];
        menuitems.remove(i);
        temp->~MenuItem();

    }

}
