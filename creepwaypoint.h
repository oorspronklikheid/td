#ifndef CREEPWAYPOINT_H
#define CREEPWAYPOINT_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"

class CreepWayPoint : public TemplateItem
{
    Q_OBJECT
public:
    CreepWayPoint(QPointF qpointf);
    CreepWayPoint(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item, Tile *arrtiles[16][10]);
    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
//    typesubItem getSubType();
    typesubItem getSubType(){return creepwaypoint;}
    typemainItem getMainType(){return waypoint;}

    int getHealth();
    void setHealth(int health);
    int setPositions(int i); //i = 0 first ,  i = 1 middle i = 2 last
    long int getID();
    void setImage(QImage *newimage,int rank);
    int getWorth(){}
    static QString getLabel(){return QString("");}
    virtual void setModifier(typemodifier newmodifier){}
    ~CreepWayPoint();
private:
//    long int ID;
    int position;
    int Health ;
    QBrush brush ;
    QImage *image[4];
};
#endif // CREEPWAYPOINT_H


