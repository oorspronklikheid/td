#ifndef MENU_H
#define MENU_H
#include "QGraphicsScene"
#include "QGraphicsItem"
#include "menuitem.h"
class Menu : public QObject
{
     Q_OBJECT
public:
    Menu(QGraphicsScene *scene, QPointF pos);
    void addLabel(QString newlabel ="" );
private:
    QGraphicsScene *refscene;
    QVector<MenuItem*> menuitems;
    QPointF Pos ;

//    MenuItem *menuitem1;
//    MenuItem *menuitem2;
//    MenuItem *menuitem3;
//    MenuItem *menuitem4;
public:
    void dostuff();
    void setLabel(QString newlabel , int menuorder);
    ~Menu();
signals:
//    void addItem(QGraphicsObject* item);
    void menuitemClicked(int);

public slots:
    void itemClicked(int);
};

#endif // MENU_H
