#ifndef LAYOUTWIDGET_H
#define LAYOUTWIDGET_H
#include "QPoint"
#include "QPointF"

class Layoutwidget
{
public:
    Layoutwidget();

    //tiles
    int getTileWidth();
    void setTileWidth(int newWidth);
    int getTileHeight();
    void setTileHeight(int newHeight);

    QPoint getTilePos(int x, int y); // returns the center point of a tile
    QPoint getTilePos(QPoint pos); // returns the center point of a tile
    QPoint getTilePos(QPointF pos); // returns the center point of a tile

    QPoint getTileNumber(int xPos, int yPos); // returns the Tile number
    QPoint getTileNumber(QPoint pos); // returns the Tile number
    QPoint getTileNumber(QPointF pos); // returns the Tile number

    int getButtonWidth();
    int getButtonHeight();
    int getLabelWidth();
    int getLabelHeight();

    int getButtonYSpacing() ;
    int getLabelYSpacing()  ;
    int getButtonXSpacing()  ;
    int getLabelXSpacing()   ;
    int getButtonOffset() ;
    int getLabelOffset()  ;
    QPoint getPos(int x , int y) ; //returns the x,y coord for where a button/lable should be

private:
    //tiles
    int TileWidth;
    int TileYPos;
    int TileWidthOffset;
    int TileHeightOffset;

    int buttonWidth;
    int buttonHeight;
    int labelWidth;
    int labelHeight;
    int buttonYSpacing ; // spacing between buttons in y direction , includes width and padding
    int labelYSpacing  ; // spacing between labels  in y direction , includes width and padding
    int buttonXSpacing  ;// spacing between buttons in x direction , includes width and padding
    int labelXSpacing   ;// spacing between labels  in x direction , includes width and padding
    int buttonOffset ;
    int labelOffset  ;
    int xpadding     ;
    int ypadding     ;

};

#endif // LAYOUTWIDGET_H
