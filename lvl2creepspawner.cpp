#include "lvl2creepspawner.h"
#include "QRectF"
#include "lvl2creep.h"
#include "creepcalc.h"
#include "QtGui"
#include "QPoint"

lvl2CreepSpawner::lvl2CreepSpawner( QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    AssignId();
    stepleftover = 0 ;
}

lvl2CreepSpawner::lvl2CreepSpawner(QPointF qpoint , Layoutwidget *newlayout, QVector<TemplateItem *> *item , Tile *arrtiles[16][10]  ): TemplateItem( qpoint,newlayout,item,arrtiles)
{
    stepleftover = 0;
    spwnd = false;
for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
        }

    setPos(qpoint);
//    ID=11;
//    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
    items = item ;
    Health = 400;
    Health = 100;
    setEnabled(false) ;
    stepleftover = 0 ;

//    qDebug() << "spwned";
}

QRectF lvl2CreepSpawner::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-10 - penWidth / 2, -10 - penWidth / 2,
                  20 + penWidth, 20 + penWidth);

}
int lvl2CreepSpawner::getHealth()
{
    return Health ;
}



void lvl2CreepSpawner::settarget(QPointF)
{

}
long int lvl2CreepSpawner::getID()
{
    return ID;
}

void lvl2CreepSpawner::Progress(qreal step)
{
if(spwnd==false)
{
    spwnd=true;
    emit spawn(new CreepCalc(QPointF(pos()),layout,items,arrTiles)) ;
}

//    update();

    stepleftover += step;

    while(stepleftover >= 50)
    {
        stepleftover = stepleftover - 50 ;
        count++ ;
    }
//    qDebug() << "step" << step;
//    qDebug() << "count" << count;
    count = count % (1 + Health/2)  ;
    if (count==0)
    {
        Health --;
//        qDebug() << "spawing";
        emit spawn(new lvl2Creep(QPointF(pos()),layout,items,arrTiles)) ;

        count ++;

    }

}

void lvl2CreepSpawner::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
           QWidget *widget)
{
//    painter->drawRect(-10, -10, 20, 20);
//    QImage image("./graphics/spawner.png");
    QImage image(":/graphics/spawner-2.png");
    painter->drawImage(QRectF(-10, -10, 20, 20),image);


}
lvl2CreepSpawner::~lvl2CreepSpawner()
{
//    qDebug() << "I DiE!1!";
}
