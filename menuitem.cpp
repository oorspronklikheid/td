#include "menuitem.h"
#include "staticlabel.h"
#include "QDebug"
#include "QGraphicsObject"
#include "QtGui"

MenuItem::MenuItem(QPointF qpoint, QString text) : QGraphicsObject()
{
    Text = text ;
    setPos(qpoint);
    update();
    //    qDebug() << pos();
    iSizeX =1 ;
    iSizeY =1 ;
    m_brush = QBrush(QColor(255,255,255,128 +127  ));

    setAcceptHoverEvents(true);
    iOrder = 0;
    //    setFlag(QGraphicsItem::ItemIsFocusable, true);
//    qDebug() <<  acceptHoverEvents();

}

void MenuItem::setiOrder(int inew)
{
    iOrder = inew ;
}

int MenuItem::getiOrder()
{
    return iOrder  ;
}


void MenuItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
//    qDebug() << "changing mode";
    emit Clicked(iOrder);
}

void MenuItem::SetSize(int x,int y)
{
    iSizeX =x ;
    iSizeY =y ;

}
void MenuItem::slotSetText(QString text)
{
    setText( text);
}

QRectF MenuItem::boundingRect() const
{
    qreal penWidth = 3;
    int height=20 * iSizeY;
    int width=100* iSizeX ;

    QRect rect(-1 * 0.5 * width - penWidth / 2 ,-1 * 0.5 * height - penWidth / 2 , width, height);
    return rect ;

}

void MenuItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{

    int height=20 *iSizeY ,width= 100 * iSizeX ;
    QRect rect(-1 * 0.5 * width,-1 * 0.5 * height , width, height);
//    qDebug() << QStyle::State_MouseOver ;

//    widget->setAttribute(Qt::WA_Hover, true);
    if(option->state & QStyle::State_MouseOver)
    {
        m_brush = QBrush(QColor(255,255,255,128 + 64 ));
//        qDebug() << "selected";
    }else{
        m_brush = QBrush(QColor(255,255,255,128 ));
//        qDebug() << "painted";
    }
    QPen pen ;
    pen.setWidth(0.5);
    //    painter->setBrush(QColor(255,255,255,128 + 64 ));
    painter->setBrush(m_brush);
    painter->setPen(pen);
    painter->drawRoundedRect(rect, 5, 5);
    painter->drawText(rect, Qt::TextWordWrap | Qt::AlignCenter,Text);


}

void MenuItem::setText(QString text)
{
    Text = text ;
    update();
}
MenuItem::~MenuItem()
{

}
