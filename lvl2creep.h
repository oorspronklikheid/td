#ifndef LVL2CREEP_H
#define LVL2CREEP_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "QBrush"
#include "creepwaypoint.h"
#include "templateitem.h"

class lvl2Creep : public TemplateItem
{
    Q_OBJECT


public:
    lvl2Creep(QPointF qpointf);
    lvl2Creep(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
    typesubItem getSubType(){return lvl2creep;}
    typemainItem getMainType(){return creep;}

    int getHealth();
    void setHealth(int health);
    long int getID();
    QPointF gettarget(){}
    void setImage(QImage *newimage,int rank);
    virtual int getWorth();
    virtual void setModifier(typemodifier newmodifier);
    typemodifier modifier;
    qreal modifiertime;
    static QString getLabel(){return QString("");}
    ~lvl2Creep();
private:
    QImage *image;
    int lastx , lasty ;
    Tile *arrTiles[16][10];
    int Health ;
    QBrush brush ;
    QPointF target ;
    TemplateItem *tTarget ;
signals:
    void finished(TemplateItem *item);


};

#endif // LVL2CREEP_H





