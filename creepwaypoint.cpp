#include "creepwaypoint.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"
CreepWayPoint::CreepWayPoint( QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    //   Health = 100 ;
//    ID=32;
//    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
}
CreepWayPoint::CreepWayPoint(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item , Tile *arrtiles[16][10]  ): TemplateItem( qpoint,layout,item,arrtiles)
{
    setPos(qpoint);
    //   Health = 100 ;
    items = item ;
//    ID=32;
//    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
    setEnabled(false) ;
    position = 1 ;
    image[0] = new QImage(":/graphics/Tower2-4.png");
    image[1] = new QImage(":/graphics/Tower2-4.png");
    image[2] = new QImage(":/graphics/Tower2-4.png");
    image[3] = new QImage(":/graphics/Tower2-4.png");

}
void CreepWayPoint::setImage(QImage *newimage,int rank)
{
    if(rank >3 || rank < 0)
    {
        qDebug() << "Error in \"void Tower::setImage(QImage *newimage,int rank)\" rank out of range ";
        return;
    }else
    {
        image[rank] = newimage;
    }


}

int CreepWayPoint::setPositions(int i )
{
    position = i ;
}

QRectF CreepWayPoint::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-5 - penWidth / 2, -5 - penWidth / 2,
                  10 + penWidth, 10 + penWidth);

}
void CreepWayPoint::setHealth(int health)
{
    Health = health ;

}

int CreepWayPoint::getHealth()
{
    return Health ;
}


void CreepWayPoint::settarget(QPointF)
{

}
long int CreepWayPoint::getID()
{
    return ID;
}
void CreepWayPoint::Progress(qreal step)
{
    //use this function to update the creep
    QPointF point;

    //setPos(pos().x()+4,pos().y() + 4);
    //    update();

}

void CreepWayPoint::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                          QWidget *widget)
{
    brush.setColor(Qt::green);

    //    painter->setBrush();
//    painter->drawRoundedRect(-5, -5, 10, 10, 2, 2);
//    if(position==0)
//    {
//        QImage image("./graphics/waypoint-3.png");
        painter->drawImage(QRectF(-5, -5, 10, 10),*image[position]);
//    }
//    if(position==1)
//    {
////        QImage image("./graphics/waypoint-4.png");
//        painter->drawImage(QRectF(-5, -5, 10, 10),image);
//    }
//    if(position==2)
//    {
////        QImage image("./graphics/endpoint-2.png");
//        painter->drawImage(QRectF(-5, -5, 10, 10),image);
//    }

    //    painter->drawRoundedRect(-10,12,20*Health / 100,4,3,3);

}
CreepWayPoint::~CreepWayPoint()
{

}
