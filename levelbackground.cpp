#include "levelbackground.h"
#include "QGraphicsObject"
#include "QtGui"
#include "QRectF"
LevelBackground::LevelBackground()
{
    update();
    int width , height;
    width = 1024 ;
    height = 640 ;
    setPos(0.5*width-15,0.5*height-15);
    image = new QImage(":/graphics/level2.png");

}
QRectF LevelBackground::boundingRect() const
{

    qreal penWidth = 1;
    int width , height;
    width = 1024 ;
    height = 640 ;
    return QRectF(-1*0.5*width - penWidth / 2, -1*0.5*height - penWidth / 2,width + penWidth, height + penWidth);
}
void LevelBackground::setBackgroundImage(QImage *newimage)
{
//    qDebug() << "received for real";
    image = newimage;
    update();

}
void LevelBackground::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{


    int width , height;
    width = 1024 ;
    height = 640 ;
    painter->drawImage(QRectF(-1*0.5*width,-1*0.5*height ,width ,height),*image);
}
