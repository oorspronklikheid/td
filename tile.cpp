#include "tile.h"
#include "templateitem.h"
#include "QtGui"
#include "QPoint"
#include "QPointF"
#include "QDebug"
#include "QGraphicsObject"
//#include "QVector"

Tile::Tile(QPointF qpoint, Layoutwidget *newlayout)/*: itemsAtTile(QVector<TemplateItem *>(1))*/
{
    layout = newlayout;
    setEnabled(false) ;
    setPos(qpoint);
    Buildable = true ;
    bpath = false;
    bnopath = false;
    TowerPresent =false;
}
void Tile::resetState()
{
    TowerPresent =false;
    bpath = false;
    bnopath = false;
    Buildable = true ;
    update();
}

void Tile::setImgPath(QImage *image)
{
//    qDebug() << "set path";
    path = image ;
    bpath = true ;
        update();
}

void Tile::setImgnoPath(QImage *image)
{
//    qDebug() << "set nonpath" ;
    nopath = image ;
    bnopath = true ;
        update();
}

void Tile::Subscribed(TemplateItem *item)
{
//    qDebug() << "adding item" << item ;
    creepsAtTile.size();
    if(item != NULL)
    creepsAtTile.append(item);
    else qDebug() << "You should give an item";

}

void Tile::unSubscribed(TemplateItem *item)
{
//    qDebug() << itemsAtTile.size();
    bool didRemove = false;
    for(int i = 0 ; i < creepsAtTile.size() ; i++)
    {
        if (item == creepsAtTile[i])
        {
            creepsAtTile.remove(i);
            didRemove = true ;
        }
    }
    if(didRemove==false)
        qDebug() << "Did not find item!";
}

void Tile::setAge(int age)
{
    iAge =age;

    //qDebug() << "iAge" << iAge;
}

QRectF Tile::boundingRect() const
{
    qreal penWidth = 1;

    return QRectF(-0.5*layout->getTileWidth() - penWidth / 2, -0.5*layout->getTileHeight() - penWidth / 2,layout->getTileWidth() + penWidth,layout->getTileHeight() + penWidth);
}
void Tile::setBuildable(bool pass)
{
    Buildable = pass ;
    update();
}

void Tile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
//    int a = 17*Health ; // 17 = 255/15
//    if (a > 255)
//        a = 255 ;
    if(Buildable == false)
        painter->setBrush(QColor(0,0,0,0));
    painter->setPen(QColor::fromRgb(0,0,0,10));
    painter->drawRect(QRectF(-0.5*layout->getTileWidth(),-0.5*layout->getTileHeight(),layout->getTileWidth(),layout->getTileHeight()));
    QString Text ;

    if(bpath && !Buildable)
    {
//        qDebug() << "drawing path";
        painter->drawImage(QRectF(-0.5*layout->getTileWidth(), -0.5*layout->getTileHeight(), layout->getTileWidth(), layout->getTileHeight()),*path);
    }
    if(bnopath && Buildable)
    {
        painter->drawImage(QRectF(-0.5*layout->getTileWidth(), -0.5*layout->getTileHeight(), layout->getTileWidth(), layout->getTileHeight()),*nopath);
    }
    //*
    QFont f = painter->font();
    f.setPointSize(8);
    painter->setFont(f);

    Text = QString("x%1\ny%2").arg(pos().x()/layout->getTileWidth()).arg(pos().y()/layout->getTileHeight());
    painter->drawText(QRectF(-12,-12,24,24), Qt::TextWordWrap | Qt::AlignCenter ,Text);
    //*/
}
