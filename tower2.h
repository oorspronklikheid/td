#ifndef TOWER2_H
#define TOWER2_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
#include "guibutton.h"
//enum typeItem { creep , tower , towerammo , null  };

class Tower2 : public TemplateItem
{
        Q_OBJECT
public:
    Tower2(QPointF);
    Tower2(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);
    Tile *arrTiles[16][10]  ;
    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);

    typesubItem getSubType(){return tower2;}
    typemainItem getMainType(){return tower;}

     int getHealth(){}
     void setHealth(int){}
     long int getID();
     QPointF gettarget(){}
     void setImage(QImage *newimage,int rank);
     void mousePressEvent(QGraphicsSceneMouseEvent *event);
     int getWorth(){}
    ~Tower2();
     virtual void setModifier(typemodifier newmodifier){}
     void arcFire(int radius,int size);

     static QString getLabel(){return QString("Buy Area");}
     static gmmode getButtonAction(){return buytower2 ;}
     static int getInitialCost(){return 200;}

private:
     qreal stepleftover;
    int count ;
    QPointF NearestCreep(QPointF point);
//    long int ID;
//    QVector<TemplateItem*>  items ;
//void addNewItem(QPointF point);
        QImage *image;

//    void setImage(QImage *newimage);
//    QImage *image;
};
#endif
