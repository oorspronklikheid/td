#include "tower.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "towerammo.h"
#include "creep.h"
#include "templateitem.h"
#include "QImage"
Tower::Tower(QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    //ID=14;
    //    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
    count=0;
    iRange = 225*225 ;
    stepleftover = 0 ;
}


Tower::Tower(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item, Tile *arrtiles[16][10]) : TemplateItem ( qpoint,newlayout,item,arrtiles)
{
    iRange = 225*225 ;

    image[0] = new QImage(":/graphics/Tower2-4.png");
    image[1] = new QImage(":/graphics/Tower2-4.png");
    image[2] = new QImage(":/graphics/Tower2-4.png");
    image[3] = new QImage(":/graphics/Tower2-4.png");

    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
            if(arrTiles[i][j]->getPassable() == false)
            {
            qreal dx ,dy ,r ;
            dx = pos().x() - arrTiles[i][j]->pos().x();
            dx = dx * dx ;
            dy = pos().y() - arrTiles[i][j]->pos().y();
            dy = dy*dy ;
            r = qSqrt(dx+dy);
            if(r <= 225)
            {
//                qDebug() << "tiles" << "x" << arrTiles[i][j]->pos().x()/64 << "Y" << arrTiles[i][j]->pos().y()/64 ;
                TilesInRange.append(arrTiles[i][j]) ;
//                qDebug() << TilesInRange.size();

            }
            }
        }

    setPos(qpoint);
    items = item ;
    count=0;

    AssignId();
    shotsFired =0;
    Mode= nearest;

    menuExist = false ;
    stepleftover = 0 ;

}

void Tower::setImage(QImage *newimage,int rank)
{
    if(rank >3 || rank < 0)
    {
        qDebug() << "Error in \"void Tower::setImage(QImage *newimage,int rank)\" rank out of range ";
        return;
    }else
    {
        image[rank] = newimage;
    }

}

void Tower::menuresult(int order)
{
//    qDebug() << "oi , it works and item has order" << order ;
     QString stemp;
    if(order == 0  )
    {
        Mode = nearest ;
         stemp = "Nearest" ;
    }
    if(order == 1  )
    {
        Mode = eldest ;
        stemp ="Eldest";
    }
    if(order == 2  )
    {
        Mode = weakest ;
        stemp ="Weakest";
    }
    if(order == 3  )
    {
        Mode = strongest ;
        stemp ="Strongest";
    }
    emit giveStatus(QString("Shots fired :%1\nMode:" + stemp ).arg(shotsFired));
    menu->~Menu();
    menuExist= false;

}


void Tower::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    //emit changeMode(GMMode);
    if(event->button() == Qt::MidButton)
    {
//        qDebug() << "yes!";
    }
    if(event->button() == Qt::LeftButton)
    {
        QString stemp;
        //enum attackmode {nearest , eldest , weakest , strongest };
        if(Mode ==nearest )
        {
            stemp = "Nearest" ;
        }else
            if(Mode ==eldest )
            {
                stemp ="Eldest";
            }else if(Mode ==weakest )
                {
                    stemp ="Weakest";
                }else
                    if(Mode ==strongest)
                    {
                        stemp ="Strongest";

                    }
        emit giveStatus(QString("Shots fired :%1\nMode:" + stemp ).arg(shotsFired));

    }
    else
    {
        if(menuExist==true)
        {
           menu->~Menu();
           menuExist=false;
        }
        menu = new Menu(this->scene(),pos());
        menuExist = true ;

        connect(menu,SIGNAL(menuitemClicked(int)),this,SLOT(menuresult(int)));
        menu->addLabel("Nearest");
        menu->addLabel("Eldest");
        menu->addLabel("Weakest");
        menu->addLabel("Strongest");
        menu->dostuff();
//        qDebug() <<"i want to change attacking mode";
//        QString stemp;
//        //enum attackmode {nearest , eldest , weakest , strongest };
//        if(Mode ==nearest )
//        {
//            stemp = "Eldest" ;
//            Mode = eldest ;
//        }else
//            if(Mode ==eldest )
//            {
//                stemp ="Weakest";
//                Mode = weakest;
//            }else
//                if(Mode ==weakest )
//                {
//                    stemp ="Strongest";
//                    Mode = strongest;
//                }else
//                    if(Mode ==strongest)
//                    {
//                        stemp ="Nearest";
//                        Mode = nearest ;
//                    }

       // qDebug()<< QString("Shots fired :%1\nMode:" + stemp ).arg(shotsFired);
    }

}

void Tower::settarget(QPointF)
{

}

long int Tower::getID()
{
    return ID;
}

QRectF Tower::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-10 - penWidth / 2, -10 - penWidth / 2,
                  20 + penWidth, 20 + penWidth);

}


void Tower::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{


    painter->setPen(QColor(255,255,255));
    if ( Mode == nearest )painter->setBrush(QColor::fromRgb(187,38,24,120));
    else if ( Mode == eldest ) painter->setBrush(QColor::fromRgb(187,123,24,120));
    else if ( Mode == weakest ) painter->setBrush(QColor::fromRgb(164,77,30,120));
    else if ( Mode == strongest) painter->setBrush(QColor::fromRgb(177,23,117,120));
    if(Mode != nearest)
    {
//    painter->drawEllipse(-10, -10, 20, 20);
//    painter->setPen(QColor(0,0,0));
//    QPen pen(QColor(0,0,0)) ;
//    pen.setWidth(1);
//    painter->setPen(pen);
//    painter->drawEllipse(-10, -10, 20, 20);
//    painter->drawArc(-5,-5,10,10,90*16,360*16*count/20.0);
    }
    if(Mode == nearest)
    {
//        QImage image("./graphics/tower1-nearest.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),*image[0]);
    }
    if(Mode == eldest)
    {
//        QImage image("./graphics/tower1-eldest.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),*image[1]);
    }
    if(Mode == strongest)
    {
//        QImage image("./graphics/tower1-strongest.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),*image[2]);
    }
    if(Mode == weakest)
    {
//        QImage image("./graphics/tower1-weakest.png");
        painter->drawImage(QRectF(-15, -15, 30, 30),*image[3]);
    }





}

void Tower::Progress(qreal step)
{
    //use this function to update the creep
    update();
    stepleftover += step;

    while(stepleftover >= 50)
    {
        stepleftover = stepleftover - 50 ;
        count++ ;
    }
    if (count>=20)
    {

        QPointF p  ;

        int i ,dx , dy;
        i = (pos().x() - p.x())*(pos().x() - p.x())  +(pos().y() - p.y())*(pos().y() - p.y());
        {
            TowerAmmo *tammo = new TowerAmmo(QPointF(pos().x(),pos().y()),layout,items,arrTiles) ;
            //enum attackmode {nearest , eldest , weakest , strongest };
            bool foundTarget =false ;
            if(Mode ==nearest )
            {
//                qDebug() <<NearestCreep() ;
                if(NearestCreep() != NULL)
                {
                    tammo->settarget(NearestCreep());
                    qreal dist;
                    dist = (NearestCreep()->x()-pos().x())*(NearestCreep()->x()-pos().x());
                    dist += (NearestCreep()->y()-pos().y())*(NearestCreep()->y()-pos().y());
                    if(iRange < dist)
                        qDebug() << "wtf is going on!" << iRange << dist;


                    foundTarget = true ;
//                    qDebug() << "peeeew1";

                }

            }
            if(Mode ==eldest )
            {
                //                qDebug() << EldestCreep();
                if(EldestCreep() != NULL)
                {
                    tammo->settarget(EldestCreep());
                    foundTarget = true ;
//                    qDebug() << "peeeew2";
                }
            }
            if(Mode ==strongest )
            {
                //                qDebug() << StrongestCreep();
                if(StrongestCreep() != NULL)
                {
                    tammo->settarget(StrongestCreep());
                    foundTarget = true ;
//                    qDebug() << "peeeew3";
                }
            }
            if(Mode ==weakest )
            {
                //                qDebug() <<WeakestCreep();
                if(WeakestCreep() != NULL)
                {
                    tammo->settarget(WeakestCreep());
                    foundTarget = true ;
//                    qDebug() << "peeeew4";
                }
            }
            if(foundTarget ==true)
            {
//                qDebug() << "pew";
                emit spawn(tammo) ;
//                qDebug() << "peeeew";
                shotsFired ++ ;
                count = 0;
//                process.start("play ~/basicdrawing/sfx3.wav");
            }
            //*/
            //            qDebug()<< QSound::isAvailable ();
            //            QSound::play("pew.wav");

            //        count=shotsFired / 20;

        }



    }
}

QPointF Tower::NearestCreep(QPointF point)
{
    qDebug() << "why is this hapening";
    qreal x1 ,x2 ,y1 ,y2 ,l, s ;
    QPointF dest ;
    x1 = point.x() ;
    y1 = point.y() ;
    s = 10000000;
    dest.setX(-10000);
    dest.setY(-10000);
    x2 = -10000;
    y2 = -10000;

    for(int i = 0 ; i < items->size();i++)
    {
        if(items->value(i)->getSubType() == lvl1creep)
        {
            x2 = items->value(i)->pos().x();
            y2 = items->value(i)->pos().y();
            l = ( x1 - x2  )*( x1 - x2  ) + ( y1 - y2 )* ( y1 - y2 )  ;
            if(l < s)
            {
                s = l ;
                dest = items->value(i)->pos();
            }

        }
    }


    return dest ;
}

TemplateItem* Tower::NearestCreep()
{
    qreal distance ,dx,dy,shortDist;
    TemplateItem *dest ;
    TemplateItem *dest2 ;
    dest2 = NULL ;
    shortDist = 1000000000;

    //qDebug() << "/////////////////////////////////////////////////////////////////////";
    //qDebug() << "Begin test code";
    int iterations=0;
    for (int i = 0 ; i < TilesInRange.size() ; i ++ )
    {

        for(int j = 0 ; j < TilesInRange[i]->creepsAtTile.size() ; j ++ )
        {
            if(TilesInRange[i]->creepsAtTile[j]->getMainType() == creep)
            {
            dx = pos().x() - TilesInRange[i]->creepsAtTile[j]->pos().x();
            dy = pos().y() - TilesInRange[i]->creepsAtTile[j]->pos().y();
            dx = dx *dx ;
            dy = dy *dy ;
            distance = (dx + dy);
            if(shortDist > distance /*&& distance < iRange*/ )
            {
                if(TilesInRange[i]->creepsAtTile[j]->scene() == 0)
                {
                    TilesInRange[i]->creepsAtTile.remove(j); //the item  has been destroyed but its still in this list
                    qDebug() << " This is a mistake";
                }
                else
                {
                shortDist = distance ;
                dest2 = TilesInRange[i]->creepsAtTile[j];
                //                qDebug() << "new distance is" << shortDist;
                }

            }
            }
            iterations++;
//            qDebug() << " found creep with " <<  TilesInRange[i]->itemsAtTile[j]->getHealth() ;
        }
    }
    //if there is a target and it is within range , return the target
    //if there is no target return NULL
    //if the target is out of range , return NULL
    int disttance;
    if(dest2 != NULL)
    {
    disttance = (dest2->pos().x()-pos().x())*(dest2->pos().x()-pos().x()) ;
    disttance += (dest2->pos().y()-pos().y())*(dest2->pos().y()-pos().y()) ;
//    qDebug() << "distance" << distance << "iRange" << iRange ;
    if(distance > iRange )
        return NULL;
    else
        return dest2 ;
    }else
    return dest2 ;


}

TemplateItem* Tower::EldestCreep()
{
    qreal distance ,dx,dy,shortDist;
    TemplateItem *dest ;
    dest = NULL ;

    int i= 0;
    while( i < items->size() )

    {
        if(items->value(i)->getMainType() == creep)// if we have a creep
        {
            dx = pos().x() - items->value(i)->pos().x();
            dy = pos().y() - items->value(i)->pos().y();
            dx = dx *dx ;
            dy = dy *dy ;
            distance = (dx + dy);
            if(distance < iRange )
            {

                dest = items->value(i);
//                qDebug() << "new distance is" << distance;
                i = items->size();
            }

        }
        i++;
    }
    return dest ;
}

TemplateItem* Tower::WeakestCreep()
{
    qreal distance ,dx,dy,Health;
    TemplateItem *dest ;
    dest = NULL ;
    Health = 100000000;
    for(int i= items->size()-1 ; i >= 0;i--) //iterate through all items
    {
        if(items->value(i)->getMainType() == creep)// if we have a creep
        {
            dx = pos().x() - items->value(i)->pos().x();
            dy = pos().y() - items->value(i)->pos().y();
            dx = dx *dx ;
            dy = dy *dy ;
            distance = (dx + dy);
            if(distance < iRange && items->value(i)->getHealth() <  Health)
            {
                Health = items->value(i)->getHealth()  ;
                dest = items->value(i);
                //                qDebug() << "new distance is" << shortDist;
            }

        }
    }
    //    qDebug() << "Weakest creep has "<<Health << "hp"<< dest;
    return dest ;
}
TemplateItem* Tower::StrongestCreep()
{
    qreal distance ,dx,dy,Health;
    TemplateItem *dest ;
    dest = NULL ;
    Health = 0;
    for(int i= items->size()-1 ; i >= 0;i--) //iterate through all items
    {
        if(items->value(i)->getMainType() == creep)// if we have a creep
        {
            dx = pos().x() - items->value(i)->pos().x();
            dy = pos().y() - items->value(i)->pos().y();
            dx = dx *dx ;
            dy = dy *dy ;
            distance = (dx + dy);
            if(distance < iRange && items->value(i)->getHealth() > Health)
            {
                Health = items->value(i)->getHealth()  ;
                dest = items->value(i);
                //                qDebug() << "new distance is" << shortDist;
            }

        }
    }
    //    qDebug() << "Strongest creep has "<<Health << "hp"<< dest;
    return dest ;
}

void Tower::setAttackmode(attackmode mode)
{
    Mode = mode;
}

Tower::~Tower()
{

}







