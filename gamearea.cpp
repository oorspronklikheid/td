
#include "templateitem.h"
#include "gamearea.h"
#include "QtGui"
#include "creep.h"
#include "lvl2creep.h"
#include "lvl2creepspawner.h"
#include "lvl2creepboss.h"
#include "creepboss.h"
#include "creepspawner.h"
#include "creepwaypoint.h"
#include "preview.h"
#include "tower.h"
#include "tile.h"

#include "towerammo.h"
#include "towerammo2.h"
#include "mygraphicsview.h"
#include "QMouseEvent"
#include "guibutton.h"
#include "mygraphicsscene.h"

#include "acidtower.h"
#include "coldtower.h"
#include "tower2.h"
//#include <templateitem.h>


GameArea::GameArea(QWidget *parent)
    : QGraphicsView(parent)
{
    timetemp = 0;
    test = 0;
    testtotal = 0 ;
    qsrand(QTime::currentTime().msec());

    play = false;
    setupLevel();


    Itower2 = new QImage(":/graphics/Tower2-new.png") ;

    Itower1[0] = new QImage(":/graphics/tower1-nearest.png") ;
    Itower1[1] = new QImage(":/graphics/tower1-eldest.png") ;
    Itower1[2] = new QImage(":/graphics/tower1-strongest.png") ;
    Itower1[3] = new QImage(":/graphics/tower1-weakest.png") ;

    Itower1ammo = new QImage(":/graphics/tower1-weakest.png") ;
    Itower2ammo = new QImage(":/graphics/tower1-weakest.png") ;

    Ilvl1creep = new QImage(":/graphics/creepy.png") ;
    Ilvl2creep = new QImage(":/graphics/lvl2creep.png") ;
    ItilePath = new QImage(":/graphics/tower1-weakest.png") ;
    ItilenonPath = new QImage(":/graphics/tower1-eldest.png") ;

    waypoint[0]  = new QImage(":/graphics/waypoint-3.png") ;
    waypoint[1]  = new QImage(":/graphics/waypoint-4.png") ;
    waypoint[2]  = new QImage(":/graphics/endpoint-2.png") ;


    timeStamp = 0;

    layout = new Layoutwidget();
    for(int i=0 ; i < 16 ; i++)
    {
        for(int j=0 ; j < 10 ; j++)
        {
            arrTiles[i][j] = new Tile(layout->getTilePos(i,j),layout) ;

        }
    }
    QGridLayout *mainLayout = new QGridLayout(this);
    scene = new MyGraphicsScene(&items,layout,this,arrTiles,QRect(0 , 0, 900, 700 )) ;
    bg = new LevelBackground();
    scene->addItem(bg);


    uiscene = new QGraphicsScene(QRect(0 , 0, 900, 700 ));
    uibg = new LevelBackground();
    uibg->setBackgroundImage(new QImage("./graphics/backgroundnew.png"));
    uiscene->addItem(uibg);

    gameoverscene = new QGraphicsScene(QRect(0 , 0, 900, 700 ));
    //    gameoverscene = new GameOverScene(this,QRect(0 , 0, 900, 650 ));
    GameOverbg = new LevelBackground();
    GameOverbg->setBackgroundImage(new QImage(":/graphics/background.png"));
    gameoverscene->addItem(GameOverbg);

    //setup the view
    for(int i=0 ; i < 16 ; i++)
    {
        for(int j=0 ; j < 10 ; j++)
        {
            scene->addItem(arrTiles[i][j]);

        }

    }

    //    view = new MyGraphicsView(scene,&items,this,arrTiles);
    view = new MyGraphicsView(scene,this);
    view->show();
    view->setScene(uiscene);

    //    view->setScene(gameoverscene);

    // add some buttons
    int height1 , height2 ;
    height1 = 645 ;
    height2 = height1 +35 ;

//    buttons.append(new GuiButton(Tower::getLabel()    ,QPoint(25+55*0 ,height1 ),Tower::getButtonAction()));
//    buttons.append(new GuiButton(Tower2::getLabel()   ,QPoint(25+55*1, height1 ),Tower2::getButtonAction()));
//    buttons.append(new GuiButton(coldTower::getLabel(),QPoint(25+55*2 ,height1 ),coldTower::getButtonAction()));
//    buttons.append(new GuiButton(acidTower::getLabel(),QPoint(25+55*3 ,height1 ),acidTower::getButtonAction()));
//    buttons.append(new GuiButton("Sell Tower"         ,QPoint(25+55*4 ,height1 ),selltower));
//    buttons.append(new GuiButton(" pointer   "        ,QPoint(25+55*5 ,height1 ),pointer));

    buttons.append(new GuiButton(Tower::getLabel()    , layout->getPos(0,0) ,Tower::getButtonAction()));
    buttons.append(new GuiButton(Tower2::getLabel()   , layout->getPos(1,0) ,Tower2::getButtonAction()));
    buttons.append(new GuiButton(coldTower::getLabel(), layout->getPos(2,0) ,coldTower::getButtonAction()));
    buttons.append(new GuiButton(acidTower::getLabel(), layout->getPos(3,0) ,acidTower::getButtonAction()));
    buttons.append(new GuiButton("Sell Tower"         , layout->getPos(4,0) ,selltower));
    buttons.append(new GuiButton(" pointer   "        , layout->getPos(5,0) ,pointer));

    for(int i =0 ; i < buttons.size() ; i++)
    {
        buttons[i]->setWidthHeight(layout->getButtonWidth(),layout->getButtonHeight());
    }

    statlabel.append(new StaticLabel(layout->getPos(0,1),"Cost "));
    statlabel.append(new StaticLabel(layout->getPos(1,1),"Cost "));
    statlabel.append(new StaticLabel(layout->getPos(4,1),"Cash "));
    statlabel.append(new StaticLabel(layout->getPos(5,1),"Next "));
    statlabel.append(new StaticLabel(layout->getPos(6,1),"0 Ticks past"));
    statlabel.append(new StaticLabel(layout->getPos(7,1),"text \n, yes very \nlarger"));
    statlabel[5]->SetSize(2,2);
    statlabel.append(new StaticLabel(layout->getPos(6,0),QString("%1 lives left").arg(lives)));
    statlabel.append(new StaticLabel(layout->getPos(2,1),"cold tower cost"));
    statlabel.append(new StaticLabel(layout->getPos(3,1),"acid tower cost"));

    //    qDebug() << uibg->width() << uibg->height() ;
    gameovermenu= new Menu(gameoverscene,QPointF(1024/2,690/2));//width = 930 ;height = 600 ;
    gameovermenu->addLabel(QString("Replay level"));
    gameovermenu->addLabel("Return to main menu");
    gameovermenu->dostuff();
    connect(gameovermenu,SIGNAL(menuitemClicked(int)),this,SLOT(Gameovermenuclicked(int)));

    menu = new Menu(uiscene,QPointF(1024/2,690/2));//width = 930 ;height = 600 ;
    tscript = new TimeScript(&items,layout,arrTiles);
    levels =  tscript->GetLevelCount();
    //    qDebug() << "GetLevelCount()" << levels;

    for(int i = 0 ; i < levels ; i++)
        menu->addLabel(QString("Start Level %1").arg(i));
    menu->addLabel("Exit game");

    connect(menu,SIGNAL(menuitemClicked(int)),this,SLOT(mainmenuclicked(int)));
    menu->dostuff();

    for(int i = 0 ; i < statlabel.size() ; i++)
    {
        scene->addItem(statlabel[i]);
    }


    preview = new Preview();
    preview->setPos(110,110);
    scene->addItem(preview);

    //connect the buttons
    for(int i = 0 ; i < buttons.size() ; i++)
    {
    connect(buttons[i],SIGNAL(changeMode(gmmode)),scene,SLOT(modeChange(gmmode)));
    }



    addItems();
    mainLayout->addWidget(view);
//    setPlay(bool state)
    connect(scene,SIGNAL(setPlay(bool)),this,SLOT(setPlay(bool)));
    connect(scene,SIGNAL(spawn(TemplateItem*)),this,SLOT(addNewItem(TemplateItem* )));
    connect(scene,SIGNAL(deleteItem(TemplateItem*)),this, SLOT(removeItem(TemplateItem*))) ;
    connect(scene,SIGNAL(notify( gmmode)),this ,SLOT(notifymodechange(gmmode)));
    connect(scene,SIGNAL(mousemoved(QPointF)),this,SLOT(mousehasmoved(QPointF)));

   // view->resize(900,950);
   // view->setGeometry(0,0,0,0);
//    view->setStyleSheet("border: 0px");
    view->setFrameShape( QFrame::NoFrame ) ;

//        qDebug()<< "view->minimumSize()"<< view->minimumSize()
//                << "view->maximumSize()"<< view->maximumSize()
//                << view->size()<< "view->sizeHint()"<< view->sizeHint();

}

void GameArea::setupLevel()
{

    lives = 5 ;
    if(statlabel.size()>7)
    statlabel[6]->setText(QString("%1 lives left").arg(lives));
    Cash = 350 ;

    ///
    Towercost  = Tower::getInitialCost();
    Tower2cost = Tower2::getInitialCost();
    Coldcost   = coldTower::getInitialCost();
    Acidcost   = acidTower::getInitialCost();
//    Towercost = 100;
//    Tower2cost = 200;
//    Coldcost = 500;
//    Acidcost = 750;
    ///
    timePast = 0;

    for(int i = 0 ; i < labels.size();i++)
    {
        scene->removeItem(labels[0]);
        labels.remove(0);

    }
}

void GameArea::creepFinished(TemplateItem *item)
{

    for(int i = 0 ; i < items.size(); i++)
    {
        if(item == items[i])
        {
            lives --;
            scene->removeItem(item);
            items.remove(i);
            //qDebug() <<"you have "<<lives << "lives left";
            statlabel[6]->setText(QString("%1 lives left").arg(lives));
            if(lives==0)
            {
                //                view->setScene(uiscene);
                view->setScene(gameoverscene);
                GameOverbg->setBackgroundImage(new QImage("./graphics/losescreen.png"));
            }
            //

        }
    }
}

void GameArea::changeImage(QImage *image, eImagetype type)
{

    //    qDebug() << " received signal" << type << imgTilePath ;

    if(type==imgTower2) Itower2 = image ;

    if(type==imgTower1Nearest) Itower1[0] =  image ;
    if(type==imgTower1Eldest) Itower1[1] =  image ;
    if(type==imgTower1Strongets)  Itower1[2] =  image ;
    if(type==imgTower1Weakest) Itower1[3] =  image ;

    if(type==imgTower1Ammo) Itower1ammo =  image ;
    if(type==imgTower2Ammo) Itower2ammo =  image ;

    if(type==imgCreep) {
        //qDebug() << "made it";
        Ilvl1creep =  image ;
    }
    if(type==imgTilePath)
    {
        ItilePath =  image ;
        for(int i=0 ; i < 16 ; i++)
        {
            for(int j=0 ; j < 10 ; j++)
            {
                arrTiles[i][j]->setImgPath(ItilePath);
                //                arrTiles[i][j]->setImgnoPath(ItilenonPath);
            }
        }
    }
    if(type==imgTilenoPath)
    {
        ItilenonPath =  image ;
        for(int i=0 ; i < 16 ; i++)
        {
            for(int j=0 ; j < 10 ; j++)
            {
                //                arrTiles[i][j]->setImgPath(ItilePath);
                arrTiles[i][j]->setImgnoPath(ItilenonPath);
            }
        }
    }

    if(type==imgCreepBeginpoint) waypoint[0]  =  image ;
    if(type==imgCreepWaypoint) waypoint[1]  =  image ;
    if(type==imgCreepEndpoint) waypoint[2]  =  image ;
}

void GameArea::mousehasmoved(QPointF point)
{
    //qDebug() << point  ;
    point = layout->getTilePos(layout->getTileNumber(point));
    preview->setPos( point ) ;
}

void GameArea::notifymodechange(gmmode mode)
{
    for(int i = 0 ; i < buttons.size();i++)
    {
        //        qDebug() << "ebnabling and disabling";
        if( buttons[i]->GetMode() == mode)
        {
            buttons[i]->enable() ;
        }
        else
            buttons[i]->disable();

    }
    if (mode == buytower )
    {
        preview->setmode(homingtower);
    }
    if (mode == buytower2)
    {
        preview->setmode(tower2);
    }
    if (mode == buycoldtower)
    {
        preview->setmode(coldtower);
    }
    if (mode == buyacidtower)
    {
        preview->setmode(acidtower);
    }
    if (mode == selltower)
    {
        preview->setmode(nullsub);
    }
    if (mode == selltower2)
    {
        preview->setmode(nullsub);
    }
    if (mode == pointer)
    {
        preview->setmode(nullsub);
    }
}

void GameArea::removeItem(TemplateItem* item)
{
    qDebug() << "selling item";
    if(item->getSubType()==homingtower)
    {
        for(int i = 0 ; i < items.size();i++)
        {
            if (item == items[i])
            {
                scene->removeItem(items[i]);
                items.remove(i);
                Cash = Cash + (Towercost /1.1) *.75 ;
                Towercost = Towercost / 1.05 ;
            }
        }
    }

    if(item->getSubType()==tower2)
    {
        for(int i = 0 ; i < items.size();i++)
        {
            if (item == items[i])
            {
                scene->removeItem(items[i]);
                items.remove(i);
                Cash = Cash + (Tower2cost /1.1) *.75 ;
                Tower2cost = Tower2cost / 1.05 ;
            }
        }
    }

    if(item->getSubType()==coldtower)
    {
        for(int i = 0 ; i < items.size();i++)
        {
            if (item == items[i])
            {
                scene->removeItem(items[i]);
                items.remove(i);
                Cash = Cash + (Coldcost /1.1) *.75 ;
                Coldcost = Coldcost / 1.05 ;
            }
        }
    }

    if(item->getSubType()==acidtower)
    {
        for(int i = 0 ; i < items.size();i++)
        {
            if (item == items[i])
            {
                scene->removeItem(items[i]);
                items.remove(i);
                Cash = Cash + (Acidcost /1.1) *.75 ;
                Acidcost = Acidcost / 1.05 ;
            }
        }
    }

    arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->setTowerPresent(false);

}

void GameArea::addNewItem(QGraphicsObject *item)
{
    qDebug() << "added item" << item;
    scene->addItem(item);
}

void GameArea::addNewItem(TemplateItem* item)   //need to refactor this function into one that
                                                // takes the cost into account and one
                                                // which does not
{

    //qDebug() << "item->getType()" << item->getType();
    //    qDebug() << "creep" << creep ;
    //    qDebug() << "creepcalc" << creepcalc ;
    //    qDebug() << "creepspawner" << creepspawner ;

     // if(arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->getTowerPresent())
     //   qDebug() << "void GameArea::addNewItem(TemplateItem* item) , theres an tower here!";
      //else
      {

    if(item->getSubType()==homingtower)
    {
        if(Cash >= Towercost)
        {
//            //Cash=Cash -Towercost ;
//            if(item->getSubType()==towerammo)
//                Cash--;
//            else
            arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->setTowerPresent(true);

            Cash=Cash -Towercost ;
            Towercost = Towercost + Towercost /10;
            items.append( item);
            scene->addItem(items.last());
            int i = items.size() - 1;
            items[i]->setImage(Itower1[0],0);
            items[i]->setImage(Itower1[1],1);
            items[i]->setImage(Itower1[2],2);
            items[i]->setImage(Itower1[3],3);
            connect(items[i],SIGNAL(spawn(TemplateItem* ))
                    ,this,SLOT(addNewItem(TemplateItem* )));
            connect(items[i],SIGNAL(giveStatus(QString))
                    ,statlabel[5],SLOT(slotSetText(QString)));

            //            qDebug() << "next tower costs"<< Towercost << "and you have " << Cash;

        }
        else
        {
            labels.append(new HoverLabel(item->pos(),"Not enough gold!"));
            scene->addItem(labels[labels.size()-1]);
        }

    }else
        if(item->getSubType()==tower2)
        {
            arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->setTowerPresent(true);

            if(Cash >= Tower2cost)
            {

               //if(item->getSubType()==towerammo2)
               //   Cash--;
               //else
                    Cash=Cash -Tower2cost ;
                Tower2cost = Tower2cost + Tower2cost /10;
                items.append( item);

                scene->addItem(items.last());
                int i = items.size() - 1;
                connect(items[i],SIGNAL(spawn(TemplateItem* )),this,SLOT(addNewItem(TemplateItem* )));
                items[i]->setImage(Itower2,0);
                //qDebug() << "oh noes";
                //            qDebug() << "next tower costs"<< Towercost << "and you have " << Cash;

            }else
            {
                labels.append(new HoverLabel(item->pos(),"Not enough gold!"));
                scene->addItem(labels[labels.size()-1]);
            }
        }else
            if(item->getSubType()==coldtower)
            {
                arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->setTowerPresent(true);

                if(Cash >= Coldcost)
                {
                    //Cash=Cash -Towercost ;
        //            if(item->getSubType()==towerammo2)
        //                Cash--;
        //            else
                        Cash=Cash -Coldcost ;
                    Coldcost = Coldcost + Coldcost /10;
                    items.append( item);

                    scene->addItem(items.last());
                    int i = items.size() - 1;
                    connect(items[i],SIGNAL(spawn(TemplateItem* )),this,SLOT(addNewItem(TemplateItem* )));
                    items[i]->setImage(Itower2,0);
                    //qDebug() << "oh noes";
                    //            qDebug() << "next tower costs"<< Towercost << "and you have " << Cash;

                }else
                {
                    labels.append(new HoverLabel(item->pos(),"Not enough gold!"));
                    scene->addItem(labels[labels.size()-1]);
                }
            }else
                if(item->getSubType()==acidtower)
            {
                arrTiles[layout->getTileNumber(item->pos()).x()][layout->getTileNumber(item->pos()).y()]->setTowerPresent(true);

                if(Cash >= Acidcost)
                {
                    qDebug() << "Placing an acid tower";
                    //Cash=Cash -Towercost ;
        //            if(item->getSubType()==towerammo2)
        //                Cash--;
        //            else
                        Cash=Cash -Coldcost ;
                    Coldcost = Coldcost + Coldcost /10;
                    items.append( item);

                    scene->addItem(items.last());
                    int i = items.size() - 1;
                    connect(items[i],SIGNAL(spawn(TemplateItem* )),this,SLOT(addNewItem(TemplateItem* )));
                    items[i]->setImage(Itower2,0);
                    //qDebug() << "oh noes";
                    //            qDebug() << "next tower costs"<< Towercost << "and you have " << Cash;

                }else
                {
                    labels.append(new HoverLabel(item->pos(),"Not enough gold!"));
                    scene->addItem(labels[labels.size()-1]);
                }
            }else
        {
            //            qDebug() << item->getType();
            items.append( item);
            scene->addItem(items.last());
            int i = items.size() - 1;
            connect(items[i],SIGNAL(spawn(TemplateItem* ))
                    ,this,SLOT(addNewItem(TemplateItem* )));
            if(items[i]->getSubType() == creepwaypoint)
            {
                items[i]->setImage(waypoint[0],0);
                items[i]->setImage(waypoint[1],1);
                items[i]->setImage(waypoint[2],2);
            }
            if(items[i]->getSubType() == lvl1creep)
            {//            dynamic_cast<Creep*>(items[i])
                connect(dynamic_cast<Creep*>(items[i]),SIGNAL(finished(TemplateItem* )),this,SLOT(creepFinished(TemplateItem*)));
                //void finished(TemplateItem *item);
                items[i]->setImage(Ilvl1creep,0);
            }
            if(items[i]->getSubType() == lvl1creepboss)
            {//            dynamic_cast<Creep*>(items[i])
                connect(dynamic_cast<CreepBoss*>(items[i]),SIGNAL(finished(TemplateItem* )),this,SLOT(creepFinished(TemplateItem*)));
                //void finished(TemplateItem *item);
                items[i]->setImage(Ilvl1creep,0);
            }
            if(items[i]->getSubType() == lvl2creep)
            {//            dynamic_cast<Creep*>(items[i])
                connect(dynamic_cast<lvl2Creep*>(items[i]),SIGNAL(finished(TemplateItem* )),this,SLOT(creepFinished(TemplateItem*)));
                //void finished(TemplateItem *item);
//                qDebug() << "setting image" << Ilvl2creep;
                items[i]->setImage(Ilvl2creep,0);
            }
//            qDebug() << items[i]->getSubType() << "lvl2creep=" << lvl2creep  << "lvl1creep=" << lvl1creep ;
            if(items[i]->getSubType() == lvl2creepboss)
            {//            dynamic_cast<Creep*>(items[i])
                connect(dynamic_cast<lvl2CreepBoss*>(items[i]),SIGNAL(finished(TemplateItem* )),this,SLOT(creepFinished(TemplateItem*)));
                //void finished(TemplateItem *item);
                items[i]->setImage(Ilvl2creep,0);
            }
            if(item->getSubType()==creepcalc)
            {
                item->Progress(100);
            }


        }
      }

}

void GameArea::next(int ticks)
{
    //    qDebug()<<"received";
    if(ticks>-1)
        statlabel[3]->setText(QString("%1 ticks").arg(ticks));
    else
    {
        statlabel[3]->setText(QString("no next event"));
        int k = 0 ;
        bool spawnersleft=false;
        for (int i = 0 ;i< items.size();i++)
        {
            if(items[i]->getMainType()==spawner)
            {
                spawnersleft=true;
                break;
            }
        }
        if(!spawnersleft)
        {
            for (int i = 0 ;i< items.size();i++)
            {
                if(items[i]->getMainType()==creep)
                    k++;
            }
            statlabel[3]->setText(QString("%1 creeps left").arg(k));
            if(k==0)
            {
                //            view->setScene(uiscene);
                view->setScene(gameoverscene);
                GameOverbg->setBackgroundImage(new QImage("./graphics/winscreen.png"));
                //            qDebug() << "K" << k;
                play = false ;
            }

        }

    }
}

void GameArea::Gameovermenuclicked(int order)
{

    if(order==0)
        mainmenuclicked(lastlevel);
    if(order==1)
        view->setScene(uiscene);
}

void GameArea::mainmenuclicked(int order)
{
    //    qDebug() << "it works" << order;
    //    qDebug() << "view->size()"<< view->size() ;

    if(order < levels )
    {
        lastlevel = order;
        view->setScene(scene);
//        play = true ;
        for(int i=0 ; i < 16 ; i++)
        {
            for(int j=0 ; j < 10 ; j++)
            {
                //                arrTiles[i][j]->~QObject();
                //                arrTiles[i][j] = new Tile(QPoint(i*30,j*30)) ;
                //                scene->addItem(arrTiles[i][j]);
                arrTiles[i][j]->resetState();
            }
        }
        statlabel[6]->setText(QString("%1 lives left").arg(lives));
        tscript = new TimeScript(&items,layout,arrTiles);
        setupLevel();
        //        qDebug() << "reached here";

        //        qDebug() << "reached here and here";
        connect(tscript,SIGNAL(setBackground(QImage*)),bg,SLOT(setBackgroundImage(QImage*)));
        connect(tscript,SIGNAL(setImage(QImage*,eImagetype)),this,SLOT(changeImage(QImage*,eImagetype)));

        tscript->start(order);
        connect(tscript,SIGNAL(spawn(TemplateItem*)),this,SLOT(addNewItem(TemplateItem*)));
        connect(tscript,SIGNAL(nextEvent(int)),this,SLOT(next(int)));
        if(items.size()>0)
            for(int i = items.size()-1 ; i>=0 ; i--)
                scene->removeItem(items[i]);
        items.remove(0,items.size());
        tscript->Progress(0);
    }
    if(order == levels )
    {
        window()->close();
       // this->~QWidget();
    }
}

void GameArea::addItems()
{
    for(int i = 0 ; i < items.size();i++)
    {

        scene->addItem(items[i]);
        connect(items[i],SIGNAL(spawn(TemplateItem* ))
                ,this,SLOT(addNewItem(TemplateItem* )));
    }
    for(int i = 0 ; i < buttons.size();i++)
    {
        scene->addItem(buttons[i]);
        //        connect(items[i],SIGNAL(spawn(TemplateItem* ))
        //                ,this,SLOT(addNewItem(TemplateItem* )));

    }
    for(int i = 0 ; i < labels.size();i++)
    {
        scene->addItem(labels[i]);
    }

}

QSize GameArea::minimumSizeHint() const
{
    return QSize(900, 900);
}

QSize GameArea::sizeHint() const
{
    return QSize(950, 720);
}

void GameArea::progress(qreal step)
{

//    qDebug()<< "view->minimumSize()"<< view->minimumSize()
//            << "view->maximumSize()"<< view->maximumSize()
//            << view->size()<< "view->sizeHint()"<< view->sizeHint();

    if(lives>0 && play)
    {
        timePast += step  ;
        timetemp += step  ;
        if(timetemp >= 50*10)
        {
            timetemp -= 50*10 ;
            //        tests5->setText(QString("%1 clicks past").arg(timePast));
            statlabel[4]->setText(QString("%1 clicks past").arg(timePast/50));
        }

        tscript->Progress(step);
        //tscript->Progress();

        if( labels.size() > 0 && labels[0]->getHealth()==0)
        {
            scene->removeItem(labels[0]);
            labels.remove(0);
        }
        for(int i = 0 ; i < labels.size();i++)
        {
            //            labels[i]->Progress();
            labels[i]->Progress(step);

        }

        statlabel[0]->setText(QString("Cost \n%1").arg(Towercost));
        statlabel[1]->setText(QString("Cost \n%1").arg(Tower2cost));
        statlabel[7]->setText(QString("Cost \n%1").arg(Coldcost));
        statlabel[8]->setText(QString("Cost \n%1").arg(Acidcost));
        statlabel[2]->setText(QString("Cash \n%1").arg(Cash));


        for(int i = 0 ; i < items.size();i++)
        {
//            if(items[i]->getSubType()==towerammo ||items[i]->getSubType()==creepspawner || items[i]->getSubType()==towerammo2 )
                if(items[i]->getMainType()==ammo || items[i]->getMainType()==spawner)
            {
                //         qDebug()<< "ammo health :" <<items[i]->getHealth() ;
                if(items[i]->getHealth() <= 0 )
                {
                    items[i]->getID();
                    TemplateItem *temp ;
                    temp = items[i] ;
                    scene->removeItem(items[i]);
                    items.remove(i);
                    temp->~TemplateItem();
                    //                qDebug() << "fail";
                    i--;
                }
            }

        }
        //        qDebug() << "cycles spared" << test <<" cycles steps" << testtotal ;
        for(int i = 0 ; i < items.size();i++)
        {
            //            if(items[i]->getType() == towerammo ||items[i]->getType() == towerammo2) //attacking item
            //            {
            //                testtotal++;
            //                test +=  items.size() - items[i]->collidingItems().size() ;
            //                if(items[i]->collidingItems().size()<20 && items.size()>100 )
            //                qDebug() <<  "\nitems[i]->collidingItems().size()" <<  items[i]->collidingItems().size() << "items[i].size()"<<  items.size()  ;

            //            }


            //            items[i]->Progress();
            items[i]->Progress(step);
//            if(items[i]->getSubType() == towerammo ||items[i]->getSubType() == towerammo2) //attacking item
            if(items[i]->getMainType() == ammo) //attacking item
            {
                QList<QGraphicsItem *> collidongList = items[i]->collidingItems() ;

                //                qDebug() << collidongList;
                for(int k = 0 ; k < collidongList.size();k++)
                {
                    int j=0;
                    if(TemplateItem* tempTemplate = dynamic_cast<TemplateItem*>(collidongList[k]))
                    {
                        j = items.indexOf(tempTemplate);
                        if(j>0 && j < items.size())

                        //                for(int j = 0 ; j < items.size();j++)
                        //                {

                        // qDebug() << "progressingj" << j;
                        if(i != j)
                        {
                            if(i < items.size())
                                //                            if(items[i]->getType() == towerammo ||items[i]->getType() == towerammo2) //attacking item
                            {


                                //                    qDebug() << "we have towerammo";
//                                qDebug() << j ;
                                if(items[j]->getMainType() == creep) //attacked item
                                {


                                    //                        qDebug() << "we have creep";
                                    int dx,dy;
                                    dx = items[j]->pos().x() -items[i]->pos().x() ;
                                    dy = items[j]->pos().y() -items[i]->pos().y() ;
                                    int d = dx*dx + dy*dy ;
                                    //this is one twoeramoo coliding with one creep , not any other type

                                    if(items[i]->collidesWithItem(items[j])==true && d <15*15)
                                    {
                                        CollideItem(items[i],items[j], &i ,  &j);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void GameArea::CollideItem(TemplateItem *unit1,TemplateItem *unit2,int *i , int *j)
{
    //                            qDebug()<< "removing item "<< i <<"and" <<j;
    //item[i] == unit1
    //item[j] == unit2
    //qDebug() << "unit1->getDamage()"<< unit1->getDamage();
    int damage =unit1->getDamage() ;
    unit2->setModifier(unit1->getModifier());
//        damage = 20 ;//min 20 max 20*6
    unit2->setHealth(unit2->getHealth() - damage );
    if(*j < *i){


        //                                qDebug() << "killing1" << items[i]->getID() << "i" << i << "j" << j;
        scene->removeItem(unit1);
        // items[i]->~QGraphicsItem();
        //                                            TemplateItem *temp ;
        //                                            temp = items[i] ;
        items.remove(*i);
        //                                            temp->~TemplateItem();
        i--;
        if(items[*j]->getHealth() <= 0 )
        {
            unitDied(unit2);
            scene->removeItem(unit2);
            items.remove(*j);
            *j--;
        }

    }else
    {

        //items[j]->setHealth(items[j]->getHealth() - damage );
        if(items[*j]->getHealth() <= 0 )
        {
            unitDied(unit2);
            scene->removeItem(unit2);
            items.remove(*j);
            *j--;
        }

        scene->removeItem(unit1);
        items.remove(*i);
        *i--;

    }


}

void GameArea::unitDied(TemplateItem *unit)
{
//    qDebug() << unit;
//    int gain = qrand()%20 + qrand()%20 ;
    int gain = unit->getWorth();
    QString tstring;
    tstring = "+ %1";
    labels.append(new HoverLabel(unit->pos(),tstring.arg(gain)));
    scene->addItem(labels[labels.size()-1]);
    Cash= Cash +gain;
}

GameArea::~GameArea()
{
//    this->parent()->~QObject();
}
