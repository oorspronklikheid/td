#ifndef HOVERLABEL_H
#define HOVERLABEL_H
#include "QRectF"
#include "QGraphicsObject"
#include "QString"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"

class HoverLabel :   public  QGraphicsObject
{
    Q_OBJECT
public:
    HoverLabel(QPointF qpoint);
    HoverLabel(QPointF qpoint, QString text);

    void Progress(qreal step);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    typesubItem getType();
    int getHealth();
    void setHealth(int health);
    long int getID();
    void setText(QString);
private:
    QString Text;
    qreal Health ;
};

#endif // HOVERLABEL_H

