#ifndef STATICLABEL_H
#define STATICLABEL_H
#include "QRectF"
#include "QGraphicsObject"
#include "QString"
#include "QPoint"
#include "QBrush"

class StaticLabel :   public  QGraphicsObject
{
    Q_OBJECT
public:
    StaticLabel(QPointF qpoint, QString text);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void setText(QString text);
    void SetSize(int x,int y);
private:
    QString Text;
    //public slots:
    int iSizeX ;
    int iSizeY ;
public slots:
    void slotSetText(QString text) ;
};

#endif // STATICLABEL_H
