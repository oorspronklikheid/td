#include "QtGui"
//#include "interfacearea.h"
#include "window.h"
#include "gamearea.h"

const int IdRole = Qt::UserRole;

Window::Window()
{
    //interfacearea = new InterfaceArea;
    game = new GameArea(this) ;
    //QGridLayout *mainLayout = new QGridLayout;
    //mainLayout->setRowStretch(0, 1);
    //mainLayout->addWidget(game,0,0);
    //setLayout(mainLayout);
    game->show();
    shapeChanged();
    penChanged();
    brushChanged();
    setWindowTitle(tr("Basic Drawing"));

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(progress()));
    timer->start(25);
//    timer->start(20);
    k=0;
}
void Window::progress()
{
//    interfacearea->progress();
    game->progress(25);
}
void Window::stopTimer()
{
    timer->stop();
}

void Window::shapeChanged()
{

}

void Window::penChanged()
{

}
void Window::brushChanged()
{

}
