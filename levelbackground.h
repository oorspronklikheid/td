#ifndef LEVELBACKGROUND_H
#define LEVELBACKGROUND_H
#include "QGraphicsObject"

class LevelBackground :  public  QGraphicsObject
{
    Q_OBJECT
public:
    LevelBackground();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);

private:
    QImage *image;
public slots:
    void setBackgroundImage(QImage *newimage);

};

#endif // LEVELBACKGROUND_H
