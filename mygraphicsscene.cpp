#include "mygraphicsscene.h"
#include "QMouseEvent"
#include "QGraphicsView"
#include "creep.h"
#include "acidtower.h"
#include "coldtower.h"
#include "tower.h"
#include "towerammo.h"
#include "tower2.h"
#include "towerammo2.h"
#include "QDebug"
#include "QGraphicsItem"
#include "QGraphicsSceneMouseEvent"

MyGraphicsScene::MyGraphicsScene(QObject *parent) :QGraphicsScene(parent)
{

}

MyGraphicsScene::MyGraphicsScene(QVector<TemplateItem *> *item   , Layoutwidget *newlayout, QWidget *parent, Tile *arrtiles[16][10] , QRectF size) : QGraphicsScene(size,parent)
{
    layout = newlayout;
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
//            qDebug() << arrTiles[i][j];
        }

    items = item;
    //setEnabled(true) ;
    Mode = selltower ;

    //    grabMouse();
    //    setMouseTracking(true);


}
void MyGraphicsScene::keyPressEvent ( QKeyEvent * keyEvent )
{
    qDebug()<< "you pressed key number:" << keyEvent->key();
    //p = 80
    if(keyEvent->key() == 80)
    setPlay(false);

}

void MyGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mouseMoveEvent(event);
//    qDebug() << "move!" << event->scenePos();

    int x , y;
    event->ignore();


    x = layout->getTilePos(layout->getTileNumber(event->scenePos())).x();
    y = layout->getTilePos(layout->getTileNumber(event->scenePos())).y();

//    qDebug() << "X" << x/layout->getTileWidth()  << "Y" << y/layout->getTileHeight  ;

    emit mousemoved(QPointF(x,y ));
}
void MyGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
//    qDebug() <<"clickey" << event->scenePos();
    setPlay(true);
    QGraphicsScene::mousePressEvent(event);
    event->ignore();
    //    event->
    if(event->button() == Qt::RightButton)
    {
        Mode = pointer;
        emit notify( pointer) ;
    }
    int x , y;
    x = layout->getTilePos(layout->getTileNumber(event->scenePos())).x();
    y = layout->getTilePos(layout->getTileNumber(event->scenePos())).y();

    QPointF point;
    point.setX(x);
    point.setY(y);



    if(Mode == buytower && event->scenePos().y() < 630 && event->scenePos().y() > 0)
    {
        if(event->button() == Qt::LeftButton)
        {
            //            qDebug() <<  "arrTiles[x/layout->getTileWidth()][y/layout->getTileHeigth]->getPassable()" << arrTiles[x/layout->getTileWidth() -1][y/layout->getTileHeight -1]->getPassable() << "x,y" <<  x/layout->getTileWidth() -1 << y/layout->getTileHeight -1 ;

            if(x/layout->getTileWidth()  > -1 && x/layout->getTileWidth()  < 16 && y/layout->getTileHeight()  > -1 && y/layout->getTileHeight() < 10  )
            {
                if(arrTiles[x/layout->getTileWidth() ][y/layout->getTileHeight() ]->getPassable() == true)
                {
                    emit spawn(new Tower(QPointF(x,y),layout,items,arrTiles)) ;


                }else
                {
                    qDebug() << "Error: Object placement on non buildable area" <<"x" << x/layout->getTileWidth() << "y" << y/layout->getTileHeight();
                }

            } else qDebug() << "Error: Object placement out of bounds" <<"x" << x/layout->getTileWidth() << "y" << y/layout->getTileHeight();
            //    Tile *arrTiles[16][10]  ;


        }
    }

    if(Mode == buytower2 && event->scenePos().y() < 600 && event->scenePos().y() > 0)
    {

        //  qDebug("you clicked the view");
        if(event->button() == Qt::LeftButton)
        {
            //            qDebug() <<  "arrTiles[x/layout->getTileWidth()][y/layout->getTileHeight()]->getPassable()" << arrTiles[x/layout->getTileWidth() -1][y/layout->getTileHeight() -1]->getPassable() << "x,y" <<  x/layout->getTileWidth() -1 << y/layout->getTileHeight() -1 ;

            if(x/layout->getTileWidth() > -1 && x/layout->getTileWidth()  < 16 && y/layout->getTileHeight()  > -1 && y/layout->getTileHeight()  < 10  )
            {
                if(arrTiles[x/layout->getTileWidth() ][y/layout->getTileHeight() ]->getPassable() == true)
                    emit spawn(new Tower2(QPointF(x,y),layout,items,arrTiles)) ;
            } else qDebug() << "Error: Object placement out of bounds" <<"x" << x/layout->getTileWidth() << "y" << y/layout->getTileHeight();
        }

    }
    if(Mode == buycoldtower && event->scenePos().y() < 600 && event->scenePos().y() > 0)
    {

        //  qDebug("you clicked the view");
        if(event->button() == Qt::LeftButton)
        {
            //            qDebug() <<  "arrTiles[x/layout->getTileWidth()][y/layout->getTileHeight()]->getPassable()" << arrTiles[x/layout->getTileWidth() -1][y/layout->getTileHeight() -1]->getPassable() << "x,y" <<  x/layout->getTileWidth() -1 << y/layout->getTileHeight() -1 ;

            if(x/layout->getTileWidth() > -1 && x/layout->getTileWidth()  < 16 && y/layout->getTileHeight()  > -1 && y/layout->getTileHeight()  < 10  )
            {
                if(arrTiles[x/layout->getTileWidth() ][y/layout->getTileHeight() ]->getPassable() == true)
                    emit spawn(new coldTower(QPointF(x,y),layout,items,arrTiles)) ;
            } else qDebug() << "Error: Object placement out of bounds" <<"x" << x/layout->getTileWidth() << "y" << y/layout->getTileHeight();
        }

    }
    if(Mode == buyacidtower && event->scenePos().y() < 600 && event->scenePos().y() > 0)
    {

        //  qDebug("you clicked the view");
        if(event->button() == Qt::LeftButton)
        {
            //            qDebug() <<  "arrTiles[x/layout->getTileWidth()][y/layout->getTileHeight()]->getPassable()" << arrTiles[x/layout->getTileWidth() -1][y/layout->getTileHeight() -1]->getPassable() << "x,y" <<  x/layout->getTileWidth() -1 << y/layout->getTileHeight() -1 ;

            if(x/layout->getTileWidth() > -1 && x/layout->getTileWidth()  < 16 && y/layout->getTileHeight()  > -1 && y/layout->getTileHeight()  < 10  )
            {
                if(arrTiles[x/layout->getTileWidth() ][y/layout->getTileHeight() ]->getPassable() == true)
                    emit spawn(new acidTower(QPointF(x,y),layout,items,arrTiles)) ;
            } else qDebug() << "Error: Object placement out of bounds" <<"x" << x/layout->getTileWidth() << "y" << y/layout->getTileHeight();
        }

    }



    if (QGraphicsItem *item = itemAt(event->scenePos()))
    {
        if(Mode == selltower && event->scenePos().y() < 630  && event->scenePos().y() > 0)
        {
            if(dynamic_cast<TemplateItem*>(item))
                if((dynamic_cast<TemplateItem*>(item))->getMainType() == tower )
                {
                    emit deleteItem(((TemplateItem*)itemAt(event->scenePos()))) ;
                }
        }

    }
}
void MyGraphicsScene::modeChange(gmmode mode)
{
    Mode = mode ;
    emit notify( mode) ;
}










