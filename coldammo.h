#ifndef COLDAMMO_H
#define COLDAMMO_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
//enum typeItem { creep , tower , towerammo , null  };

class coldAmmo: public TemplateItem
{    Q_OBJECT
 public:
     coldAmmo(QPointF qpoint);
     coldAmmo(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrTiles[16][10]);
     QRectF boundingRect() const;
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
     void Progress(qreal step);
     void settarget(QPointF);
     void settarget(TemplateItem *item);

      typesubItem getSubType(){return coldammo;}
      typemainItem getMainType(){return ammo;}

     int getHealth();
     void setHealth(int health);
      int getWorth(){}

     long int getID();
     QPointF gettarget();
     void setImage(QImage *newimage,int rank){}
     int getDamage();
      virtual void setModifier(typemodifier newmodifier){}
      virtual typemodifier getModifier(){return cold;}
      static QString getLabel(){return QString("");}
          ~coldAmmo();

 private:
      QPointF NearestCreep(QPointF point);
      QPointF target ;
       TemplateItem *iTarget;
       qreal stepleftover;
       int Count;
//      int target ;
       int Health ;
//       QVector<TemplateItem*>  items ;
;
};

#endif // COLDAMMO_H

