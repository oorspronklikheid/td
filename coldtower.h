#ifndef COLDTOWER_H
#define COLDTOWER_H
enum coldattackmode {cnearest , celdest , cweakest , cstrongest };
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
#include "tile.h"
#include "QProcess"
#include "menu.h"
#include "QWheelEvent"
#include "guibutton.h"
class coldTower : public TemplateItem
{
        Q_OBJECT
public:

    coldTower(QPointF);
    coldTower(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);

    typesubItem getSubType(){return coldtower;}
    typemainItem getMainType(){return tower;}

     int getHealth(){}
     void setHealth(int){}
     long int getID();
     QPointF gettarget(){}
     void mousePressEvent(QGraphicsSceneMouseEvent *event);


    void setAttackmode(coldattackmode mode);
    ~coldTower();
     void setImage(QImage *newimage,int rank);
     virtual void setModifier(typemodifier newmodifier){}
     int getWorth(){}

     static QString getLabel(){return QString("Buy Cold");}
     static gmmode getButtonAction(){return buycoldtower ;}
     static int getInitialCost(){return 500;}
private:

    Tile *arrTiles[16][10];
    QVector<Tile *> TilesInRange ;
    int iRange ;
    coldattackmode Mode ;
    unsigned int shotsFired;

    qreal stepleftover;
    int count ;

    QPointF NearestCreep(QPointF point);
    TemplateItem* NearestCreep();
    TemplateItem* EldestCreep();
    TemplateItem* WeakestCreep();
    TemplateItem* StrongestCreep();
    QProcess process;

    QImage *image[4];
    Menu *menu;
    bool menuExist ;

     //    long int ID;
     //    QVector<TemplateItem*>  items ;
     //void addNewItem(QPointF point);
public slots:
    void menuresult(int);

};

#endif // COLDTOWER_H
