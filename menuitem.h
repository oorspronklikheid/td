#ifndef MENUITEM_H
#define MENUITEM_H
#include "QRectF"
#include "QGraphicsObject"
#include "QString"
#include "QPoint"
#include "QBrush"

class MenuItem :   public  QGraphicsObject
{
    Q_OBJECT
public:
    MenuItem(QPointF qpoint, QString text);
    ~MenuItem();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void setText(QString text);
    void SetSize(int x,int y);
    void setiOrder(int inew);
    int getiOrder();
private:
    int iOrder; //the order of the item in the menu
    QString Text;
    //public slots:
    int iSizeX ;
    int iSizeY ;
    QBrush m_brush;
protected:
 //   virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
 //   virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

public slots:
    void slotSetText(QString text) ;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
signals:
    void Clicked(int);

};

#endif // MENUITEM_H



