#ifndef CREEP_H
#define CREEP_H
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "QBrush"
#include "creepwaypoint.h"
#include "templateitem.h"

class Creep : public TemplateItem
{
    Q_OBJECT
public:
    Creep(QPointF qpointf);
    Creep(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
    typesubItem getSubType(){return lvl1creep;}
    typemainItem getMainType(){return creep;}

    int getHealth();
    void setHealth(int health);
    long int getID();
    QPointF gettarget(){}
    void setImage(QImage *newimage,int rank);
    virtual int getWorth();
    virtual void setModifier(typemodifier newmodifier);
    typemodifier modifier;
    qreal modifiertime;
    static QString getLabel(){return QString("");}
    ~Creep();
private:
    QImage *image;
    int lastx , lasty ;
    Tile *arrTiles[16][10];
    int Health ;
    QBrush brush ;
    QPointF target ;
    TemplateItem *tTarget ;
signals:
    void finished(TemplateItem *item);


};

#endif // CREEP_H

