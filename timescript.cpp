#include "timescript.h"
#include "QFile"
#include "QDebug"
#include "QRegExp"
#include "creep.h"
#include "lvl2creep.h"
#include "lvl2creepspawner.h"
#include "lvl2creepboss.h"
#include "creepboss.h"
#include "creepspawner.h"
#include "creepwaypoint.h"
#include "creepcalc.h"
#include "tower.h"
#include "tower2.h"
#include "towerammo.h"
#include "towerammo2.h"
#include "coldtower.h"

TimeScript::TimeScript(QVector<TemplateItem *> *varitem , Layoutwidget *newlayout , Tile *arrtiles[16][10])
{
    layout = newlayout;
    tickpart =0;
    items = varitem ;
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
        }
    timeStamp = 1 ;
    if(ReadlevelData("./leveldata.txt") < 0)
        qDebug() << "Warning , function ReadlevelData(\"./leveldata.txt\") reported error/s!";

}

void TimeScript::start(int levelnum)
{

    if(Readlevel(levelnum) < 0 )
        qDebug() << "Warning , function Readlevel(1) reported error/s!";

    if( /*TimeScript::*/sortVectScript() < 0 )
        qDebug() << "Warning , \"int TimeScript::sortVectScript()\" reported error/s!";
    //    qDebug() << VectScriptDelay;
    //    qDebug() << VectScriptTypeitem;
    //    qDebug() << VectScriptpoint;
    //    qDebug() << "LevelDataLevelDescription[0]" << LevelDataLevelDescription[0] ;
    //    qDebug() << "LevelDataLevelFilePath[0]" << LevelDataLevelFilePath[0] ;
    //    qDebug() << "LevelDataLevelNumber[0]" << LevelDataLevelNumber[0] ;

}

int TimeScript::Progress(qreal step)
{
//    qDebug() << "int TimeScript::Progress(qreal step)" << "timeStamp" << timeStamp << "step" << step;
//    tickpart += step;
    timeStamp += step/50;
//    while(tickpart >= 50)
    {
//    timeStamp++;
//    tickpart = tickpart + 1 ;
    }
//    if(timeStamp %10 == 0 && VectScriptDelay.size() >= 0)
    if(timeStamp - tickpart  >= 10 && VectScriptDelay.size() >= 0)
    {
        tickpart++;
//    timeStamp =timeStamp -10;
//          qDebug() << "timeStamp" << timeStamp << "VectScriptDelay[0]"<<VectScriptDelay[0]  ;
        //  statlabel[3]->setText(QString("%1 \nClicks").arg(VectScriptDelay[0]-timeStamp));
          if(VectScriptDelay.size()>0)
              emit nextEvent( (int(VectScriptDelay[0]-timeStamp)/10)*10)   ;
          else
              emit nextEvent(-1)   ;
//          timeStamp++;
    }

    if(VectScriptDelay.size() == 0)
    {
        //statlabel[3]->setText(QString("no next event"));
        //test if ther are zero creeps
        bool iscreeps = false;
        for (int i= 0 ; i < items->size(); i++)
        {
            if(items->at(i)->getSubType()==lvl1creep)
            {
                iscreeps = true ;
                break;
            }

        }
        //        if(iscreeps == false )
        //            statlabel[3]->setText(QString("You are finished"));
        // done
    }

    if(VectScriptDelay.size() > 0)
        if(timeStamp >= VectScriptDelay[0] )
        {
//            qDebug() << "aaah" << timeStamp << VectScriptTypeitem[0];
//            qDebug() << "creepwaypoint =" <<creepwaypoint << "; creepcalc =" <<creepcalc;
            if(VectScriptTypeitem[0] == homingtower)
            {
                emit spawn(new Tower(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == coldtower)
            {
                emit spawn(new coldTower(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == tower2)
            {
                emit spawn(new Tower2(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == towerammo)
            {
                emit spawn(new TowerAmmo(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == towerammo2)
            {
                emit spawn(new TowerAmmo2(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == lvl1creepspawner)
            {
                qDebug() << "adding a spawner";
                emit spawn(new CreepSpawner(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == lvl2creepspawner)
            {
                emit spawn(new lvl2CreepSpawner(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == creepwaypoint)
            {
                for(int i = 0 ; i < items->size() ; i++)
                {
                    if(items->value(i)->getSubType() == creepwaypoint)
                    {
                        if(dynamic_cast<CreepWayPoint*>(items->value(i)))
                        {
                           (dynamic_cast<CreepWayPoint*>(items->value(i)))->setPositions(1);

                        }
                    }
                }
                for(int i = 0 ; i < items->size() ; i++)
                {
                    if(items->value(i)->getSubType() == creepwaypoint)
                    {
                        if(dynamic_cast<CreepWayPoint*>(items->value(i)))
                        {
                           (dynamic_cast<CreepWayPoint*>(items->value(i)))->setPositions(0);
                            break;
                        }
                    }
                }

                emit spawn(new CreepWayPoint(VectScriptpoint[0],layout,items,arrTiles));
                (dynamic_cast<CreepWayPoint*>(items->value(items->size()-1)))->setPositions(2);
            }
            if(VectScriptTypeitem[0] == lvl1creep)
            {
                emit spawn(new Creep(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == lvl1creepboss)
            {
                qDebug() << "spawning a lvl1Creepboss";
                emit spawn(new CreepBoss(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == lvl2creep)
            {
                emit spawn(new lvl2Creep(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == lvl2creepboss)
            {
                qDebug() << "spawning a lvl2Creepboss";
                emit spawn(new lvl2CreepBoss(VectScriptpoint[0],layout,items,arrTiles));
            }
            if(VectScriptTypeitem[0] == creepcalc)
            {
                emit spawn(new CreepCalc(VectScriptpoint[0],layout,items,arrTiles));
            }


//            scene->addItem(items.last());
//            connect(items.last(),SIGNAL(spawn(TemplateItem* ))
//                    ,this,SLOT(addNewItem(TemplateItem* )));
//            qDebug() << "Adding item";

            if(VectScriptDelay.size() > 0)
                VectScriptRemove(0);
            if(VectScriptDelay.size() > 0)
                if(timeStamp >= VectScriptDelay[0] )
                {
//                    timeStamp -- ;
//                    qDebug() << "Adding another element" ;
                    //if(VectScriptTypeitem[0] ==homingtower)qDebug() << "homingtower" ;
                    Progress(0);
                }
        }
}

int TimeScript::sortVectScript()
{
    //seems to be correct
    typesubItem tempitem = nullsub;
    unsigned long int tempdelay;
    QPointF temppoint ;

    for(int i=0; i<VectScriptDelay.size()-1 ; i++)
    {
        for(int j=0; j <VectScriptDelay.size()-1 ; j++)
        {
            if(VectScriptDelay[j] > VectScriptDelay[j+1])
            {
                // save vars to temp
                tempdelay = VectScriptDelay[j+1];
                tempitem =  VectScriptTypeitem[j+1];
                temppoint = VectScriptpoint[j+1];

                //move elment j to j+1
                VectScriptDelay[j+1] =  VectScriptDelay[j];
                VectScriptTypeitem[j+1] = VectScriptTypeitem[j] ;
                VectScriptpoint[j+1] = VectScriptpoint[j] ;

                //move temp element to j
                VectScriptDelay[j] = tempdelay;
                VectScriptTypeitem[j] = tempitem  ;
                VectScriptpoint[j] = temppoint;
                //                hold=array[j];
                //                array[j]=array[j+1];
                //                array[j+1]=hold;
            }
        }
    }
    //    QVector<unsigned long int >  VectScriptDelay;
    //    QVector<typeItem>  VectScriptTypeitem;
    //    QVector<QPointF>  VectScriptpoint;
    return 0 ;
}

int TimeScript::Readlevel(int Ilevel)
{

    int ipos1 = - 1;
    for(int i =0 ; i< LevelDataLevelNumber.size() ; i ++)
    {
        if(Ilevel == LevelDataLevelNumber[i])
        {
            ipos1 = i;
            break ;
        }

    }
    //    qDebug() << ipos1;
    if ( ipos1 < 0 )
    {
        qDebug() << "Error in int TimeScript::Readlevel(int Ilevel) , unable to find level in array";
        return -1 ;
    }
//    qDebug() << "Ilevel" << Ilevel ;
//    qDebug() << LevelDataLevelFilePath;
//    qDebug() << LevelDataLevelNumber ;
//    qDebug() << LevelDataLevelFilePath[ipos1];
    QFile file(LevelDataLevelFilePath[ipos1]);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error in int TimeScript::Readlevel(int Ilevel) , Unable to open file " + file.fileName() ;
        return -2;
    }

    QString line;
    int linesRead = 0;

    while (!file.atEnd())
    {
        linesRead++;
        line = file.readLine();
        if(line.contains("\n")== true)
        {
            line = line.remove("\n");
        }
        if(line.startsWith("//")== false)
        {


            if(line.startsWith("level")==false && line.startsWith("end")==false )
            {
                //                qDebug() << line;
                bool identifiedline = false ;
                int timestamp;
                if(line.startsWith("+"))
                {
                    //qDebug() << "relative";
                    // recalulate to absolute
                    int pos2 = -1 ;
                    pos2 = line.indexOf(" ");
                    QString stemp;
                    stemp = line ;
                    stemp = stemp.remove(pos2,line.length()) ;
                    stemp = stemp.remove(0,1) ;
                    //qDebug() << pos2 << stemp ;
                    int relative = -1 ;
                    relative = stemp.toInt();
                    timestamp = relative ;
                    line.remove(0,pos2+1);
                    if(VectScriptDelay.size()> 0)
                    {
                        timestamp += VectScriptDelay[VectScriptDelay.size()-1];
                    }
                    //if() if previous line exist , add it to timestamp , else add nothing

                }else
                {
                    //qDebug() << "absolute";
                    int pos2 = -1 ;
                    pos2 = line.indexOf(" ");
                    QString stemp;
                    stemp = line ;
                    stemp = stemp.remove(pos2,line.length()) ;
                    //stemp = stemp.remove(0,1) ;
                    //qDebug() << stemp.toInt()  << line;
                    timestamp = stemp.toInt();
                    line.remove(0,pos2+1);
                }

                if(line.contains("spawn"))
                {
                    line.remove("spawn ");
                    ReadActionSpawn(timestamp,line);
                    identifiedline = true ;
                }
                if(line.contains("modify"))
                {
                    ReadActionModify(timestamp,line);
                    identifiedline = true ;
                }
                if(line.contains("resource "))
                {
                    //resource background ./graphics/level1.png
                    ReadResource(timestamp,line);
                    identifiedline = true ;
                }


                if(identifiedline == false)
                {
                    qDebug() << "Error in int TimeScript::Readlevel(int Ilevel) , unable to match action for line" << linesRead ;
                    qDebug() << line;
                    return -3;
                }

            }
        }

    }
    return 0 ;
}

int TimeScript::ReadActionSpawn(int iTime, QString line)
{
    if(iTime < 0 )
    {
        qDebug() << "Error in int TimeScript::ReadActionSpawn(int iTime, QString line), invalid time received";
        return -1;
    }
    int ix , iy ;
    int pos1 , pos2 ;
    bool xOk,yOk ;
    xOk=true;
    QString stemp ;
    pos1 = line.indexOf(" ");
    pos2 = line.indexOf(" ",pos1+1);
    stemp = line;
    stemp.remove(pos2,stemp.length()-pos2);
    stemp.remove(0,pos1+1);
    ix = stemp.toInt(&xOk);
    //    qDebug() << "stemp" << stemp ;
    stemp = line;
    stemp.remove(0,pos2+1);
    iy = stemp.toInt(&yOk);
    //    qDebug() << line << ix << iy;
    if(xOk ==false || yOk == false)
    {
        if(xOk==false)
        {
            qDebug() << "Error in \" int TimeScript::ReadActionSpawn(int iTime, QString line) \" unable to convert x value ";
            qDebug() << "Offending line was:" <<line;
        }
        if(yOk==false)
        {
            qDebug() << "Error in \" int TimeScript::ReadActionSpawn(int iTime, QString line) \" unable to convert y value ";
            qDebug() << "Offending line was:" <<line;

        }
        return -2 ;

    }
    bool didspawn=false ;
    //void addNewItem(TemplateItem *item);
    if(line.contains("lvl1creepspawner "))
    {
        //emit spawn(new CreepSpawner(QPointF(ix,iy),items,arrTiles)) ;
//        qDebug() << "err i believe i saw a spawner";
        didspawn= true ;

        if( VectScriptAdd (iTime, lvl1creepspawner ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }


    }
    if(line.contains("creepwaypoint "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, creepwaypoint ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("lvl1creep "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, lvl1creep ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("lvl1creepboss "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, lvl1creepboss ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
//        qDebug() << "spawned a boss";
    }
    if(line.contains("lvl2creep "))
    {
//        qDebug() << "added lvl2 creep";
        didspawn= true ;
        if( VectScriptAdd (iTime, lvl2creep ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("lvl2creepspawner "))
    {
//        qDebug() << "added lvl2 creep";
        didspawn= true ;
        if( VectScriptAdd (iTime, lvl2creepspawner ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("lvl2creepboss "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, lvl2creepboss ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
//        qDebug() << "spawned a boss";
    }
    if(line.contains("tower1 ")||line.contains("homingtower "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, homingtower ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("coldtower "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, coldtower ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("tower2 "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, tower2 ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("tower1ammo "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, towerammo ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("tower2ammo "))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, towerammo2 ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }
    if(line.contains("creepcalc"))
    {
        didspawn= true ;
        if( VectScriptAdd (iTime, creepcalc ,QPointF(ix * layout->getTileWidth(),iy * layout->getTileHeight()))  < 0)
        {
            qDebug() << "Warning function \"if( VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) < 0) \" returened erros";
        }
    }


    if(didspawn== false)
    {
        qDebug() << "Error in function int TimeScript::ReadActionSpawn(int iTime, QString line) , could not match item type ";
        qDebug() << "Offending line is " << line;
        return -3;
    }
    return 0 ;
}

int TimeScript::ReadResource(int iTime , QString line)
{


    if(iTime < 0 )
    {
        qDebug() << "Error in \"int TimeScript::ReadResource(int iTime , QString line)\", invalid time received";
        return -1;
    }
    line.remove("resource ");
    bool resourceIdentified=false;
    if(line.contains("background "))
    {
        line.remove("background ");
//        qDebug() << "new background is:" << line;
        emit setBackground(new QImage(line) ) ;
        resourceIdentified = true ;
    }
    if(line.contains("imgTower1Weakest "))
    {
        line.remove("imgTower1Weakest ");
        emit setImage(new QImage(line),imgTower1Weakest);
        resourceIdentified = true ;
    }
    if(line.contains("imgTower1Strongets "))
    {
        line.remove("imgTower1Strongets ");
        emit setImage(new QImage(line),imgTower1Strongets);
        resourceIdentified = true ;
    }
    if(line.contains("imgTower1Nearest "))
    {
        line.remove("imgTower1Nearest ");
        emit setImage(new QImage(line),imgTower1Nearest);
        resourceIdentified = true ;
    }

    if(line.contains("imgTower1Eldest "))
    {
        line.remove("imgTower1Eldest ");
        emit setImage(new QImage(line),imgTower1Eldest);
        resourceIdentified = true ;
    }
    if(line.contains("imgTower2 "))
    {
        line.remove("imgTower2 ");
        emit setImage(new QImage(line),imgTower2);
        resourceIdentified = true ;
    }
    if(line.contains("imgTower1Ammo "))
    {
        line.remove("imgTower1Ammo ");
        emit setImage(new QImage(line),imgTower1Ammo);
        resourceIdentified = true ;
    }
    if(line.contains("imgTower2Ammo "))
    {
        line.remove("imgTower2Ammo ");
        emit setImage(new QImage(line),imgTower2Ammo);
        resourceIdentified = true ;
    }
    if(line.contains("imgCreepBeginpoint "))
    {
        line.remove("imgCreepBeginpoint ");
        emit setImage(new QImage(line),imgCreepBeginpoint);
        resourceIdentified = true ;
    }
    if(line.contains("imgCreepWaypoint "))
    {
        line.remove("imgCreepWaypoint ");
        emit setImage(new QImage(line),imgCreepWaypoint);
        resourceIdentified = true ;
    }
    if(line.contains("imgCreepEndpoint "))
    {
        line.remove("imgCreepEndpoint ");
        emit setImage(new QImage(line),imgCreepEndpoint);
        resourceIdentified = true ;
    }
    if(line.contains("imgCreep "))
    {
        line.remove("imgCreep ");
        emit setImage(new QImage(line),imgCreep);
        resourceIdentified = true ;
    }
    if(line.contains("imgTilePath "))
    {
        line.remove("imgTilePath ");
        emit setImage(new QImage(line),imgTilePath);
        resourceIdentified = true ;
//        qDebug() << "emmited << imgTilePath";
    }
//    if(line.contains("imgTilePath "))
//    {
//        line.remove("imgTilePath ");
//        emit setImage(new QImage(line),imgTilePath);
//        resourceIdentified = true ;
//    }

    if(line.contains("imgTilenoPath "))
    {
        line.remove("imgTilenoPath ");
        emit setImage(new QImage(line),imgTilenoPath);
        resourceIdentified = true ;
//        qDebug() << "emmited << imgTilePath";
    }

//    qDebug() << "lol";

    if(resourceIdentified == false)
    {
        qDebug() << "Error in \"int TimeScript::ReadResource(int iTime , QString line)\", unable to match resource identifier";
        qDebug() <<  line;
        return -2 ;
    }
    return 0 ;
}

int TimeScript::VectScriptAdd(unsigned long int delay, typesubItem item , QPointF point )
{
    if(VectScriptDelay.size() == VectScriptTypeitem.size() && VectScriptTypeitem.size()  == VectScriptpoint.size() )
    {
        VectScriptDelay.append(delay);
        VectScriptTypeitem.append(item);
        VectScriptpoint.append(point);
        return 0 ;
    } else
    {
        qDebug() << "Error in function \"int TimeScript::VectScriptAdd(unsigned long int delay, typeItem item , QPointF point ) \" size mismatch" ;
        qDebug() << VectScriptDelay.size() << VectScriptTypeitem.size() << VectScriptpoint.size() ;
        return -1 ;

    }
}

int TimeScript::VectScriptRemove(int i)
{
    if(i <= VectScriptDelay.size())
    {
        if(VectScriptDelay.size() == VectScriptTypeitem.size() && VectScriptTypeitem.size()  == VectScriptpoint.size() )
        {
            VectScriptDelay.remove(i);
            VectScriptTypeitem.remove(i);
            VectScriptpoint.remove(i);
            return 0 ;
        }else
        {
            qDebug() << "Error in function \"int TimeScript::VectScriptRemove(int i) \" size mismatch" ;
            qDebug() << VectScriptDelay.size() << VectScriptTypeitem.size() << VectScriptpoint.size() ;
            return -1 ;
        }
    }else
    {
        qDebug() << "Error in function \"int TimeScript::VectScriptRemove(int i) \" element i is out of range" ;
        qDebug() << "i" << i << "size" << VectScriptDelay.size() ;
        return -2 ;
    }

}

int TimeScript::levelDataAdd(int number , QString filepath ,  QString description)
{
    if(LevelDataLevelNumber.size() == LevelDataLevelFilePath.size() && LevelDataLevelNumber.size()  == LevelDataLevelDescription.size() )
    {
        LevelDataLevelNumber.append(number);
        LevelDataLevelFilePath.append(filepath);
        LevelDataLevelDescription.append(description);
        return 0 ;
    } else
    {
        qDebug() << "Error in function \" \" size mismatch" ;
        qDebug() << LevelDataLevelNumber.size() << LevelDataLevelFilePath.size() << LevelDataLevelDescription.size() ;
        return -1 ;

    }

}

int TimeScript::levelDataRemove(int i)
{
    if(i >= LevelDataLevelNumber.size())
    {
        if(LevelDataLevelNumber.size() == LevelDataLevelFilePath.size() && LevelDataLevelNumber.size()  == LevelDataLevelDescription.size() )
        {
            LevelDataLevelNumber.remove(i);
            LevelDataLevelFilePath.remove(i);
            LevelDataLevelDescription.remove(i);
            return 0 ;
        }else
        {
            qDebug() << "Error in function \"int TimeScript::levelDataRemove(int i) \" size mismatch" ;
            qDebug() << LevelDataLevelNumber.size() << LevelDataLevelFilePath.size() << LevelDataLevelDescription.size() ;
            return -1 ;
        }
    }else
    {
        qDebug() << "Error in function \"int TimeScript::levelDataRemove(int i) \" element i is out of range" ;
        qDebug() << "i" << i << "size" << LevelDataLevelNumber.size() ;
        return -2 ;
    }

}

int TimeScript::ReadActionModify(int iTime, QString line)
{
    if(iTime < 0 )
    {
        qDebug() << "Error in \"int TimeScript::ReadActionModify(int iTime, QString line) \", invalid time received";
        return -1;
    }
    return 0 ;
}

int TimeScript::ReadActionDelete(int iTime, QString line)
{
    if(iTime < 0 )
    {
        qDebug() << "Error in \"int TimeScript::ReadActionDelete(int iTime, QString line) \", invalid time received";
        return -1;
    }
    return 0 ;
}

int TimeScript::GetLevelCount()
{
    return LevelDataLevelNumber.size();
}

int TimeScript::ReadlevelData(QString path)
{
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error in  TimeScript::ReadlevelData(QString path) , Unable to open file " + file.fileName() ;
        return -1;
    }
    QString line ;
    int linesRead = 0;
    while (!file.atEnd())
    {
        linesRead++;
        line = file.readLine();
        if(line.contains("\n")== true)
        {
            line = line.remove("\n");
        }
        //      QRegExp rx("(\\ |\\,|\\.|\\:|\\t)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        if(line.contains("//")== false)
        {
            if(line.startsWith("add"))
                if(ReadAddLine(line)<0)
                {
                    qDebug() << "Warning , function ReadAddLine reported error/s!, lines read:" << linesRead;

                }

        }
    }
    return 1 ;
}

int TimeScript::ReadAddLine(QString line)
{
        //qDebug() << "found add line" << line;
    line = line.remove("add ");
    //    qDebug() << line;
    if(line.count("\"") !=2)
    {
        qDebug() << "Error in void TimeScript::ReadAddLine(QString line); malformed add line. check parameter description";
        return -1;
    }
    int pos1 , pos2 , pos3 ;
    pos1 = line.indexOf("\"");
    pos2 = line.indexOf("\"",pos1+1);
    if(pos1 < 0 || pos2 < 0 )
    {
        qDebug() << "Error in void TimeScript::ReadAddLine(QString line); malformed add line. check parameter description";
        return -2;
    }
    QString desc;
    desc = line ;
    desc = desc.remove(0,pos1 + 1 ); // remove bit befor description
    pos3 = desc.indexOf("\"");
    if(pos3 > 0)
    {
        desc.chop(desc.length()-pos3);
    }
    //    qDebug() << "desc" << desc ;
    line = line.remove(pos1-1,pos2-pos1 +2 ) ;
    //    qDebug() << "line" << line ;

    //time to seperate the file path and level number
    QString sNumber , sPath ;
    pos1 = line.indexOf(" ");
    sNumber = line ;
    sPath   = line ;
    sNumber.chop(sNumber.length() - pos1);
    sPath.remove(0,pos1 +1);
    //    qDebug() << "sNumber" << sNumber << "sPath" << sPath ;
    //    qDebug() << pos1 ;
    //    qDebug() << "sNumber.toInt()" << sNumber.toInt() ;

    QFile file2(sPath);
    if (!file2.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error in  TimeScript::ReadAddLine(QString line) , Unable to open file " + file2.fileName() ;

        return -1;
    }
    else
    {
        //qDebug() <<"if(levelDataAdd(sNumber.toInt() ,sPath, desc) < 0 )" << sNumber.toInt();
        if(levelDataAdd(sNumber.toInt() ,sPath, desc) < 0 )
        {
            qDebug() << "Warning function \" levelDataAdd(sNumber.toInt() ,sPath, desc) < 0  \" returned errors";
        }


    }

    file2.close();

    return 1 ;
}


/*
command format is
time action (parameters)

0 spawn tower1 x y
+100 spawn creep
+0 modify creep x y attackpower ivalue
0 modify creep x y position x2 y2
0 modify any x y enabled bool

==time==
can be either in xxx or +xxx
xxx signifies ticks since game started
+xxx signifies time since previous event has passed
so :
0 == start of game
+0 == imediatly after the previous object


==action==
spawn   ;  spawns object listed at position
modify  ; modify an object after its been laced
delete  ; deletes object at location



file format for level data:
// Some comments
// and descriptions
// this will be ignored
// but only if the line starts with it

level 1 "description of levels"
0 spawn creepwaypoint 9 3
0 spawn creepwaypoint 10 2
0 spawn creepwaypoint 19 2
0 spawn creepwaypoint 19 15
0 spawn creepwaypoint 11 15
0 spawn creepwaypoint 11 5
0 spawn creepwaypoint 12 4
0 spawn creepwaypoint 13 5
0 spawn creepwaypoint 13 19
0 spawn creepwaypoint 30 19
0 spawn creepwaypoint 0 0
0 spawn creepwaypoint 0 19
0 spawn creepspawner 9 10
+2800 spawn creepspawner  8 10
+0 spawn creepspawner 10 10
+2800 spawn creepspawner 8 10
+0 spawn creepspawner 10 10
+0 spawn creepspawner 9 11
+0 spawn creepspawner 9 9
end level 1

file format for ./leveldata.txt
// Some comments
// and descriptions
// this will be ignored
// but only if the line starts with it
// this files name is reserved
add 1 ./levels/file1.txt "this is a easy level"
add 2 ./levels/file2.txt "you may have to use a brain cell"
add 3 ./levels/file3.txt "clicking is required!!"
add 4 ./levels/file4.txt "survival"
*/
