#include "towerammo2.h"
#include "creep.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "templateitem.h"
TowerAmmo2::TowerAmmo2(QPointF qpoint): TemplateItem( qpoint)
{
    setPos(qpoint);
    Health = 5; //100 / 20 = 5
    AssignId();
    Count = 0 ;

}
TowerAmmo2::TowerAmmo2(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item , Tile *arrtiles[16][10] ): TemplateItem( qpoint,newlayout,item ,arrtiles)
{
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
        }
    setPos(qpoint);
    Health = 8; //100 / 20 = 5
    items = item ;
    AssignId();
//    target = NearestCreep(pos());
    Count = 0 ;
     stepleftover =0;
}
int TowerAmmo2::getDamage()
{
    return 25 ;
}



QPointF TowerAmmo2::gettarget()
{
    return target ;
}

QRectF TowerAmmo2::boundingRect() const
{
    qreal penWidth = 1;
      return QRectF( -14 - penWidth / 2, -10 - penWidth / 2, 20 , 20) ;
}


void TowerAmmo2::settarget(QPointF point)
{
    target = point ;
//    qDebug() << "void TowerAmmo::settarget(QRectF)" << "traget is : " << target ;
}
void TowerAmmo2::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    int length ;
    length =10 ;
    QVector<QPointF> points;
//    points << QPointF( 0, -length ) << QPointF( -length/1.414, length/1.414 ) << QPointF( length/1.414, length/1.414)  ;
//    painter->drawPolygon(points);

    painter->drawEllipse(QPoint(0,0),3,3);
}

int TowerAmmo2::getHealth()
{
    return Health ;
}
void TowerAmmo2::setHealth(int health)
{
    Health = health  +10 ;
}
long int TowerAmmo2::getID()
{
    return ID;
}
void TowerAmmo2::Progress(qreal step)
{
    stepleftover += step ;
    while(stepleftover >= 50)
    {
        stepleftover = stepleftover - 50 ;
//        Count++ ;
        Health-- ;
    }


//    qDebug() << "Health" << Health ;
    qreal dx , dy ;
    int max = 20 ;
//    dx = target.x() - pos().x() ;
//    dy = target.y() - pos().y() ;
//    if (dx > max)
//    {
//        dx = max ;
//    }

//    if (dy > max)
//    {
//        dy = max ;
//    }
//    if (dx < -max)
//    {
//        dx = -max ;
//    }
//    if (dy < -max)
//    {
//        dy = -max ;
//    }
///////////
    qreal dr = max*(step/50);
    qreal r,theta;

//        target = NearestCreep(pos());
        dx = target.x() - pos().x() ;
        dy = target.y() - pos().y() ;

    r = qSqrt(dx*dx +dy*dy);

    if(r < dr)
        dr = r ;// t/a = tan theta
    // t = y
    // a = -x
    // -y/x = tan theta
    // tan-1(-y/x) = theta

    theta = qAtan2(dx,dy);
//    qDebug() << "theta" << theta;

    dx =  dr*qSin(theta);
    dy =  dr* qCos(theta);
//    qDebug() << "dr" << dr;
//    qDebug() << "dx" << dx << "dy"<< dy;
    /////////

    setPos(pos().x() + 0.9*dx,pos().y() +0.9*dy);
    update();


}

TowerAmmo2::~TowerAmmo2()
{
//    qDebug() << "smells like something died";
}
