#ifndef LVL2CREEPSPAWNER_H
#define LVL2CREEPSPAWNER_H
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"

class lvl2CreepSpawner : public TemplateItem //QGraphicsItem
{
    Q_OBJECT

public:
    lvl2CreepSpawner(QPointF qpointf);
    lvl2CreepSpawner(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item,Tile *arrtiles[16][10] );
    Tile *arrTiles[16][10]  ;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
    typesubItem getSubType(){return lvl2creepspawner;}
    typemainItem getMainType(){return spawner;}

    int count;
    qreal stepleftover;
    int getHealth();
    void setHealth(int){}
    long int getID();
    QPointF gettarget(){}
    void setImage(QImage *newimage,int rank){}
    int getWorth(){}
    virtual void setModifier(typemodifier newmodifier){}
    ~lvl2CreepSpawner();
    static QString getLabel(){return QString("");}
private:
    bool spwnd;
    int Health ;
//    long int ID;
//    QVector<TemplateItem*> items ;

};

#endif // LVL2CREEPSPAWNER_H






