#ifndef TEMPLATEITEM_H
#define TEMPLATEITEM_H

#include "QGraphicsItem"
#include "QGraphicsObject"
#include "layoutwidget.h"
//#include "tile.h"
enum typesubItem { lvl1creep, lvl1creepboss, lvl2creepspawner , lvl2creep,lvl2creepboss , creepcalc , lvl1creepspawner , creepwaypoint , homingtower , coldtower ,tower2,towerammo2 , towerammo , coldammo , nullsub , acidtower, acidammo  };
enum typemainItem { creep, calc , spawner , waypoint , tower ,ammo, nullmain  };
enum typemodifier {acid , cold , nomodifier} ;
class Tile;
class TemplateItem :  public  QGraphicsObject//public QObject , public  QGraphicsItem
{
    Q_OBJECT
public:
    TemplateItem(QPointF);
    TemplateItem(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*>*,Tile *arrTiles[16][10] );

    virtual QRectF boundingRect() const{return QRectF(0,0,0,0);}

    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
    //QWidget *widget);
    virtual void Progress(qreal step)=0 ;
    QVector<TemplateItem *>* GetItem();
     virtual ~TemplateItem();
    virtual void settarget(QPointF point)=0;
    virtual QPointF gettarget(){}

    virtual typesubItem getSubType()=0 ;
    virtual typemainItem getMainType()=0 ;

    virtual int getHealth(){}
    virtual void setHealth(int){}
    virtual long int getID(){return 99;} //returns the ID of a item
    QVector<TemplateItem*> *items ;
    void AssignId();
    long int ID;
    virtual void setImage(QImage *newimage,int rank)=0;
    virtual int getDamage(){}
    virtual int getWorth()=0;

    virtual void setModifier(typemodifier newmodifier)=0;
    virtual typemodifier getModifier(){}
    void setAge(qreal age){qAge = age;}
    qreal getAge(void){return qAge;}

    //virtual static QString getLabel()=0;

    Layoutwidget *layout;

signals:
    void spawn(TemplateItem* item);
    void giveStatus(QString sStatus);

private:
    QRectF target;
    qreal qAge;

public slots:


};
#endif // TEMPLATEITEM_H

