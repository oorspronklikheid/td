#ifndef GUIBUTTON_H
#define GUIBUTTON_H
enum gmmode { buytower , selltower , buytower2 , selltower2 , pointer , buycoldtower , buyacidtower} ;

#include "QGraphicsItem"
#include "QGraphicsObject"
#include "QMouseEvent"


class GuiButton : public  QGraphicsObject//public QObject , public  QGraphicsItem
{
    Q_OBJECT
public:
    GuiButton();
    GuiButton(QString text, QPoint qpoint, gmmode md, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void Progress(qreal step);
    void enable();
    void disable();
    QRectF boundingRect() const;
    gmmode GetMode();

    void setWidthHeight(int newwidth , int newheight);
private:
    QString Text;
    gmmode GMMode;
    bool active;
    int width;
    int height;
signals:
    void clicked();
    void changeMode(gmmode);
public slots:
   virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // GUIBUTTON_H
