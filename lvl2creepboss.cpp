#include "lvl2creepboss.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"
#include "QVector"
#include "tile.h"

lvl2CreepBoss::lvl2CreepBoss( QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    Health = 5000 ;
    AssignId();



}
lvl2CreepBoss::lvl2CreepBoss(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10] ): TemplateItem( qpoint,newlayout,item,arrtiles)
{
    modifier = nomodifier ;
    lastx = layout->getTileNumber(pos()).x();
    lasty = layout->getTileNumber(pos()).y();
//    qDebug() << "this is " << this;
//    qDebug() << "lastx" << lastx << "lasty" << lasty;


    setEnabled(false) ;
    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
            qreal dx ,dy ,r ;
        }
    setPos(qpoint);
    arrTiles[lastx][lasty]->Subscribed(this);
    Health = 90000 ;
//    Health = 300 ;
    items = item ;
    AssignId();
    for(int i= items->size()-1 ; i >= 0 ; i--)
    {
        if(items->value(i)->getSubType() == creepwaypoint)
        {
            target = items->value(i)->pos();
//            if(dynamic_cast<CreepWayPoint*>(item->value(i)))
            tTarget = items->value(i);
//            qDebug() << "tTarget" << tTarget;
        }

    }
}

int lvl2CreepBoss::getWorth()
{
    int i = qrand()%1601 + qrand()%1601 + qrand()%1601 + qrand()%1601 + qrand()%1601 + qrand()%1601 ;
    qDebug() << "Gold:" << i;
    return i ;
}

void lvl2CreepBoss::setModifier(typemodifier newmodifier)
{
    if(newmodifier ==cold){
    modifier = newmodifier ;
    modifiertime = 10 ;
    }
    if(newmodifier ==acid){
        modifier = newmodifier ;
        modifiertime = 40 ;
        }
}

void lvl2CreepBoss::setImage(QImage *newimage,int rank)
{
    image = newimage;
//    qDebug() << "who disturbs my slumber";
}

long int lvl2CreepBoss::getID()
{
    return ID;
}

QRectF lvl2CreepBoss::boundingRect() const
{
    int size = 1.0 *110 ;
    return QRectF(-1*0.5*size, -1*0.5*size, size , size);

}
void lvl2CreepBoss::setHealth(int health)
{
    Health = health ;
    if(Health <= 0)
        arrTiles[lastx][lasty]->unSubscribed(this);

}

int lvl2CreepBoss::getHealth()
{
    return Health ;
}



void lvl2CreepBoss::settarget(QPointF)
{

}

void lvl2CreepBoss::Progress(qreal step)
{
    //use this function to update the creep
    if(modifiertime>0)
    {
        modifiertime = modifiertime - step / 50.0 ;
    }
    else
    {
        modifier = nomodifier;
    }

    bool didfind=false;
    if(tTarget->pos() ==pos())
    {
//        qDebug() << "I have reached the target and need a new one" ;
//        qDebug() << j ;
        int d = -1 ;

        for(int i=0 ; i < items->size();i++)
        {
            if(tTarget == items->value(i))
            {
//                qDebug() << "Found my destination :P";
                d = i ;
            }

        }
//        qDebug() << "looking for next waypoint";
        didfind=false;
        for(int i = items->size()-1 ; i>d  ;i--) //we want the next waypoint
        {
//            qDebug() << "pew" << i;
            if(items->value(i)->getSubType() == creepwaypoint)
            {

                tTarget = items->value(i);
                didfind = true;
            }
        }

    }
    if(didfind==false && tTarget->pos() ==pos())
    {
        emit finished(this);
//        qDebug() << "emit signal to remove this guy and remove a live";
    }
    if (lastx != layout->getTileNumber(pos()).x() || lasty != layout->getTileNumber(pos()).y())
    {
        arrTiles[lastx][lasty]->unSubscribed(this);
        lastx = layout->getTileNumber(pos()).x() ;
        lasty = layout->getTileNumber(pos()).y() ;
        arrTiles[lastx][lasty]->Subscribed(this);
//        qDebug() << "I'm at: x=" << layout->getTileNumber(pos()).x() << " y=" << layout->getTileNumber(pos()).y() ;

    }

    qreal dx , dy ;
    qreal max = (step/35.0) ;
    if(modifier == cold)
        max = (max ) -  (3.0*max/4)*(modifiertime/10); // modifiertime == 100 => max = max/3  modifiertime == 0 => max = max
    if(modifier == acid)
    {
        max = 2*max ;
        if(Health > 5)
        Health=Health -3;
    }
    //    dx = target.x() - pos().x() ;
    //    dy = target.y() - pos().y() ;
    dx = tTarget->pos().x() - pos().x() ;
    dy = tTarget->pos().y() - pos().y() ;

    if (dx > max) dx = max ;
    if (dy > max) dy = max ;
    if (dx < -max) dx = -max ;
    if (dy < -max) dy = -max ;

    setPos(pos().x() + dx,pos().y() +dy);
    // setPos(pos().x()+4,pos().y() + 4);

//    update();

}

void lvl2CreepBoss::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    brush.setColor(Qt::green);


//    painter->setBrush(QColor(196,196,196));
//    painter->drawRoundedRect(-10, -10, 20, 20, 5, 5);
    qreal size;
    size = 1.0* Health / 90000 * 80 + 20;
    painter->drawImage(QRectF(-1*0.5*size, -1*0.5*size, size , size),*image);

//    qreal red , green ;
//    green = 255 *Health / 120000 ;
//    red = 255 - green ;
//    painter->setBrush(QColor(red,green,0));
//    painter->setPen(QColor(0,0,0));
//    painter->drawRoundedRect(-10,12,20*Health / 30000,5,3,3);
    //    painter->drawText(QRectF(-10,-12,20,10),"Creep");


}
lvl2CreepBoss::~lvl2CreepBoss()
{
//    qDebug() << "smells like something died";
}
