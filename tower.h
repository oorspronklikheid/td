#ifndef TOWER_H
#define TOWER_H
enum attackmode {nearest , eldest , weakest , strongest };
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
#include "tile.h"
#include "QProcess"
#include "menu.h"
#include "QWheelEvent"
#include "guibutton.h"
class Tower : public TemplateItem
{
        Q_OBJECT
public:
    Tower(QPointF);
    Tower(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);

    typesubItem getSubType(){return homingtower;}
    typemainItem getMainType(){return tower;}

     int getHealth(){}
     void setHealth(int){}
     long int getID();
     QPointF gettarget(){}
     void mousePressEvent(QGraphicsSceneMouseEvent *event);


    void setAttackmode(attackmode mode);
    ~Tower();
     void setImage(QImage *newimage,int rank);
     virtual void setModifier(typemodifier newmodifier){}
     int getWorth(){}

     static QString getLabel(){return QString("Buy Homing");}
     static gmmode getButtonAction(){return buytower ;}
     static int getInitialCost(){return 100;}

private:

    Tile *arrTiles[16][10];
    QVector<Tile *> TilesInRange ;
    int iRange ;
    attackmode Mode ;
    unsigned int shotsFired;

    qreal stepleftover;
    int count ;

    QPointF NearestCreep(QPointF point);
    TemplateItem* NearestCreep();
    TemplateItem* EldestCreep();
    TemplateItem* WeakestCreep();
    TemplateItem* StrongestCreep();
    QProcess process;

    QImage *image[4];
    Menu *menu;
    bool menuExist ;

     //    long int ID;
     //    QVector<TemplateItem*>  items ;
     //void addNewItem(QPointF point);
public slots:
    void menuresult(int);
};
#endif
