#include "tower2.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "towerammo.h"
#include "towerammo2.h"
#include "creep.h"
#include "templateitem.h"
Tower2::Tower2(QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
//ID=14;
//    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
    count=0;
    stepleftover = 0 ;
}
Tower2::Tower2(QPointF qpoint , Layoutwidget *newlayout, QVector<TemplateItem*> *item,Tile *arrtiles[16][10] ) : TemplateItem ( qpoint,layout,item,arrtiles)
{
    image = new QImage (":/graphics/Tower2-new.png");

    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
        }

    setPos(qpoint);
    items = item ;
    count=0;
    stepleftover = 0 ;
//ID=13;
//    qDebug()<<"Hi , I am" <<getType() ;
    AssignId();
}

void Tower2::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug() << "i felt that!";
}

void Tower2::settarget(QPointF)
{

}

long int Tower2::getID()
{
    return ID;
}

QRectF Tower2::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-15 - penWidth / 2, -15 - penWidth / 2,
                  30 + penWidth, 30 + penWidth);

}


void Tower2::setImage(QImage *newimage,int rank)
{
    //qDebug() << "updated image" ;
    image = newimage ;
    update();
}

void Tower2::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{

//    painter->drawEllipse(-10, -10, 20, 20);

//    painter->drawArc(-5,-5,10,10,90*16,360*16*count/20.0);
    int radius = 30 ;
    double  ratio = 2 ;
    int points= 8 ;
//    qreal x,y ;
    qreal circ = 2 * 3.14159 ;
    qreal increment = circ*1.0/8 ;
    qreal offset = increment * 0.5 ;
    QPointF pointss[16];

    for (int i = 0 ;i <  8; i++)
    {
        pointss[2*i] = QPointF(radius*qCos(increment*i),radius*qSin(increment*i));
        pointss[2*i+1] = QPointF(ratio*radius*qCos(increment*i + offset ),ratio*radius*qSin(increment*i + offset));
    }
//    painter->drawPolygon(pointss,12);

    painter->drawImage(QRectF(-15, -15, 30, 30),*image);


}

void Tower2::arcFire(int radius,int size)
{
    qreal circ = 2 * 3.14159 ;
    qreal increment = circ*1.0/(8*size) ;
    qreal offset = 1 * increment * 0.5 ;


    for (int i = 0 ;i <  8; i++)
    {
//                TowerAmmo2 *tammo1 = new TowerAmmo2(QPointF(pos().x()+15,pos().y()+5),items) ;
        TowerAmmo2 *tammo1 = new TowerAmmo2(QPointF(pos().x(),pos().y()),layout,items,arrTiles) ;
        tammo1->settarget( QPointF(pos().x() + radius*qCos(increment*i +offset), pos().y() +radius*qSin(increment*i +offset)));
        emit spawn(tammo1) ;
    }
}

void Tower2::Progress(qreal step)
{
    //use this function to update the creep
    update();

    stepleftover += step;
//    qDebug() << step;
    while(stepleftover >= 50)
    {
        stepleftover = stepleftover - 50 ;
        count++ ;
    }
//    if(count <=20)
//    count++ ;
//    qDebug()<<count;
//    count = count % 20  ;
    if (count>=20)
    {
//        qDebug() << "fiiire";
        QPointF p  ;
        p = NearestCreep(pos());
        int distance;
        distance = (pos().x() - p.x())*(pos().x() - p.x())  +(pos().y() - p.y())*(pos().y() - p.y());
        //////
        int radius = 160 ;

        //////

        if( distance < radius*radius ) //if theres
        {
            arcFire(radius,1);

//            qDebug()<< QSound::isAvailable ();
//            QSound::play("pew.wav");
        count=0;
        }



    }
}

QPointF Tower2::NearestCreep(QPointF point)
{
    qreal x1 ,x2 ,y1 ,y2 ,l, s ;
    QPointF dest ;
    x1 = point.x() ;
    y1 = point.y() ;
    s = 10000000;
    dest.setX(-10000);
    dest.setY(-10000);
    x2 = -10000;
    y2 = -10000;
    for(int i = 0 ; i < items->size();i++)
    {
        if(items->value(i)->getMainType() == creep)
        {
            x2 = items->value(i)->pos().x();
            y2 = items->value(i)->pos().y();
            l = ( x1 - x2  )*( x1 - x2  ) + ( y1 - y2 )* ( y1 - y2 )  ;
            if(l < s)
            {
                s = l ;
                dest = items->value(i)->pos();
            }

        }
    }


    return dest ;
}

Tower2::~Tower2()
{

}







