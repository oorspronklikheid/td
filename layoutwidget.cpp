#include "layoutwidget.h"
#include "QDebug"
Layoutwidget::Layoutwidget()
{
    //load defaults
    TileYPos = 64;
    TileWidth = 64;
    TileWidthOffset  =0;
    TileHeightOffset =0;

    xpadding     = 5;
    ypadding     = 5;
    buttonWidth  = 50;
    buttonHeight = 30;
    labelWidth   = buttonWidth;
    labelHeight  = buttonHeight;
    buttonYSpacing = TileYPos *10 + ypadding ;
    labelYSpacing  = buttonYSpacing +buttonHeight +ypadding ;
    buttonXSpacing  = buttonWidth +xpadding;
    labelXSpacing   = buttonXSpacing; //we want them to be the same
    buttonOffset = buttonXSpacing*0.5;
    labelOffset  = buttonOffset; //we want them to be the same
    //done loading defaults
}

int Layoutwidget::getTileWidth()
{
    return TileWidth;
}

void Layoutwidget::setTileWidth(int newWidth)
{
    TileWidth = newWidth ;
}

int Layoutwidget::getTileHeight()
{
    return TileYPos;
}

void Layoutwidget::setTileHeight(int newHeight)
{
    TileYPos = newHeight ;
}

QPoint Layoutwidget::getTilePos(int x, int y)
{
    return QPoint(x*TileWidth+ TileWidthOffset,y * TileYPos +TileHeightOffset);
}
QPoint Layoutwidget::getTilePos(QPoint pos)
{
    return getTilePos(pos.x(),pos.y());
}

QPoint Layoutwidget::getTilePos(QPointF pos)
{
    qDebug()<<"QPoint Layoutwidget::getTilePos(QPointF pos)";
    return getTilePos(pos.x(),pos.y());
}

QPoint Layoutwidget::getTileNumber(int xPos, int yPos)
{
    //xPos = x*TileWidth+ TileWidthOffset ;
    return  QPoint((xPos +0.5*TileWidth - TileWidthOffset)/TileWidth,(yPos +0.5*TileYPos - TileHeightOffset)/TileYPos);
}

QPoint Layoutwidget::getTileNumber(QPoint pos)
{
    return getTileNumber(pos.x(),pos.y());
}
QPoint Layoutwidget::getTileNumber(QPointF pos)
{
    return getTileNumber(pos.x(),pos.y());
}

int Layoutwidget::getButtonYSpacing()
{
    return buttonYSpacing ;
}

int Layoutwidget::getLabelYSpacing()
{
    return labelYSpacing ;
}

int Layoutwidget::getButtonXSpacing()
{
    return buttonXSpacing ;
}

int Layoutwidget::getLabelXSpacing()
{
    return labelXSpacing ;
}

int Layoutwidget::getButtonOffset()
{
    return buttonOffset;
}

int Layoutwidget::getLabelOffset()
{
    return labelOffset ;
}

QPoint Layoutwidget::getPos(int x, int y)
{
    QPoint point ;

    point = QPoint(buttonOffset + x*buttonXSpacing,labelYSpacing);
    if(y==0)
        point = QPoint(buttonOffset + x*buttonXSpacing,buttonYSpacing);
    if(x==7)
        point = QPoint(buttonOffset + (x+0.5)*buttonXSpacing,(labelYSpacing+buttonYSpacing)/2);
    return point;
}

int Layoutwidget::getButtonWidth()
{
    return buttonWidth ;
}

int Layoutwidget::getButtonHeight()
{
    return buttonHeight ;
}

int Layoutwidget::getLabelWidth()
{
    return labelWidth ;
}

int Layoutwidget::getLabelHeight()
{
    return labelHeight ;
}
