#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H
#include "QGraphicsView"
#include "templateitem.h"
#include "guibutton.h"
class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit MyGraphicsView(QGraphicsScene *scene, QWidget *parent);
    //Creep(QPointF qpoint, QVector<TemplateItem *> *item);
    explicit MyGraphicsView(QGraphicsScene *scene, QVector<TemplateItem *> *item  ,QWidget *parent = 0,Tile *arrtiles[16][10] =NULL);
    Tile *arrTiles[16][10]  ;
    QVector<TemplateItem*> *items ;
signals:
    void spawn(TemplateItem* item);
    void deleteItem(TemplateItem* item);
    void notify(gmmode mode);
    void mousemoved(QPointF);
public slots:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void modeChange(gmmode mode);

private:
    gmmode Mode;
};

#endif // MYGRAPHICSVIEW_H
