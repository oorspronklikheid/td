#ifndef CREEPSPAWNER_H
#define CREEPSPAWNER_H
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"

class CreepSpawner : public TemplateItem //QGraphicsItem
{
    Q_OBJECT
public:
    CreepSpawner(QPointF qpointf);
    CreepSpawner(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item,Tile *arrtiles[16][10] );
    Tile *arrTiles[16][10]  ;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
    typesubItem getSubType(){return lvl1creepspawner;}
    typemainItem getMainType(){return spawner;}

    int count;
    qreal stepleftover;
    int getHealth();
    void setHealth(int){}
    long int getID();
    QPointF gettarget(){}
    void setImage(QImage *newimage,int rank){}
    int getWorth(){}
    virtual void setModifier(typemodifier newmodifier){}
    static QString getLabel(){return QString("");}
    ~CreepSpawner();
private:
    bool spwnd;
    int Health ;
//    long int ID;
//    QVector<TemplateItem*> items ;
};

#endif // CREEPSPAWNER_H


