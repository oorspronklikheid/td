#include "guibutton.h"
#include "QMouseEvent"
#include "QtGui"
#include "QPoint"
#include "QWidget"
#include "QGraphicsObject"
#include "QGraphicsScene"

GuiButton::GuiButton()
{

}
GuiButton::GuiButton(QString text , QPoint qpoint, gmmode md, QGraphicsItem *parent) : QGraphicsObject(parent)
{

    setPos(qpoint);
    update();
    Text = text ;
    GMMode = md ;
    active = false;
  //  setAcceptedMouseButtons(Qt::LeftButton);
//    setMouseTracking(true);
    width = 50;
    height = 30;


}
gmmode GuiButton::GetMode()
{
    return GMMode ;
}

void GuiButton::setWidthHeight(int newwidth, int newheight)
{
    width = newwidth ;
    height= newheight;
}

void GuiButton::enable()
{
    active = true ;
    update();

}
void GuiButton::disable()
{
    active = false ;
    update();
}

void GuiButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    emit changeMode(GMMode);
}

void GuiButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                      QWidget *widget)
{
    //int height=30 ,width=50 ;
    QRect rect(-1 * 0.5 * width,-1 * 0.5 * height , width, height);

    QPen pen ;
    pen.setWidth(3);

    if(active)
        painter->setBrush(QColor::fromRgb(200,200,200));
    else
        painter->setBrush(QColor::fromRgb(255,255,255));


    float factor = 1.0*rect.width() / painter->fontMetrics().width(Text) * 1.4;
    if(factor > 0)
    if ((factor < 1) || (factor > 1.25))
    {
        QFont f = painter->font();
        f.setPointSizeF(f.pointSizeF()*factor);
        painter->setFont(f);
    }
//    QPen pen ;
//    pen.setWidth(2);

    painter->setPen(pen);
    painter->drawRoundedRect(rect, 5, 5);
    painter->drawText(rect, Qt::TextWordWrap | Qt::AlignCenter,Text);

//    painter->setPen(pen);
//    painter->drawRoundedRect(-1 * 0.5 * width,-1 * 0.5 * height, width, height, 5, 5);
//    painter->drawText( -1 * 0.5 * width, -1 * 0.5 * height, width, height, Qt::TextWordWrap | Qt::AlignCenter,Text);

}
void GuiButton::Progress(qreal step)
{

}

QRectF GuiButton::boundingRect() const
{
    int height=30 ,width=50 ;
    qreal penWidth = 3;
//    return QRectF(-16 - penWidth / 2, -16 - penWidth / 2, 32 + penWidth, 32 + penWidth);

    QRect rect(-1 * 0.5 * width - penWidth / 2 ,-1 * 0.5 * height - penWidth / 2 , width, height);
    return rect ;
}
