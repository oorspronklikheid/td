#ifndef GAMEAREA_H
#define GAMEAREA_H

#include "QWidget"
#include "QGraphicsView"
#include "QGraphicsScene"
#include "QGraphicsEllipseItem"
#include "QGridLayout"
#include "creep.h"
#include "creepcalc.h"
#include "tile.h"
#include "tower.h"
#include "QVector"
#include "guibutton.h"
#include "hoverlabel.h"
#include "staticlabel.h"
#include "preview.h"
#include "timescript.h"
#include "levelbackground.h"
#include "menuitem.h"
#include "menu.h"
#include "layoutwidget.h"

//enum typeItem { creep , tower , towerammo , null  };

class GameArea :  public QGraphicsView
{
    Q_OBJECT

public:
    GameArea(QWidget *parent = 0);
    ~GameArea();
    QGraphicsView *view;
    QGraphicsScene *scene;
    QGraphicsScene *uiscene;
    QGraphicsScene *gameoverscene;
//    GuiButton *button ;

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

    QGridLayout *mainLayout ;
    void progress(qreal step);
private:
    void CollideItem(TemplateItem *unit1,TemplateItem *unit2,int *i , int *j);
    void unitDied(TemplateItem* unit);
    void setupLevel();

    bool play;
    void loadFromFile();
    void timeScript();
    QVector<unsigned long int >  VectScriptDelay;
    QVector<typesubItem>  VectScriptTypeitem;
    QVector<QPointF>  VectScriptpoint;
    unsigned long int timeStamp;
    qreal timePast;
    qreal timetemp;
    //QPointF NearestCreep(QPointF point);
    void addItems();
    QGraphicsEllipseItem *ellipseItem;
    QVector<TemplateItem*> items ;
    QVector<GuiButton*> buttons ;
    QVector<HoverLabel*> labels ;
    QVector<Tile> *VectTiles;
    Tile *arrTiles[16][10]  ;
//    CreepCalc *creepcalc;
    int lastlevel;
    int lives ;
    int Cash ;
    int Towercost;
    int Tower2cost;
    int Coldcost;
    int Acidcost;
    int levels;

    QVector<StaticLabel*>  statlabel;
    Preview *preview;

    TimeScript *tscript;
    LevelBackground *bg;
    LevelBackground *uibg;
    LevelBackground *GameOverbg;

    QImage *Itower1[4] ;
    QImage *Itower2 ;
    QImage *Itower1ammo ;
    QImage *Itower2ammo ;
    QImage *Ilvl1creep ;
    QImage *Ilvl2creep ;
    QImage *waypoint[3];
    QImage *ItilePath ;
    QImage *ItilenonPath ;
    QImage *images[13];

    Menu *menu;
    Menu *gameovermenu;

    unsigned long int test;
    unsigned long int testtotal;

    Layoutwidget *layout;
public slots:
    void setPlay(bool state){play = state;}
    void creepFinished(TemplateItem *item);
    void mousehasmoved(QPointF point);
    void addNewItem(TemplateItem *item);
    void addNewItem(QGraphicsObject *item);
    void mainmenuclicked(int order);
    void Gameovermenuclicked(int order);

    void removeItem(TemplateItem *item);
    void notifymodechange(gmmode mode);
    void changeImage(QImage *image, eImagetype type);
    void next(int ticks);
//    void addNewItem(TemplateItem *item);

};

#endif // GAMEAREA_H
