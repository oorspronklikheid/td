#ifndef CREEPCALC_H
#define CREEPCALC_H
#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "QBrush"
#include "creepwaypoint.h"
#include "templateitem.h"


class CreepCalc : public TemplateItem
{
    Q_OBJECT
public:
    CreepCalc(QPointF qpointf);
    CreepCalc(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void Progress(qreal step);
    void settarget(QPointF);
//    typesubItem getSubType();
    typesubItem getSubType(){return creepcalc;}
    typemainItem getMainType(){return calc;}

    int getHealth();
    void setHealth(int health);
    long int getID();
    QPointF gettarget(){}
    void setImage(QImage *newimage,int rank){}
    int getWorth(){};
    virtual void setModifier(typemodifier newmodifier){}
    static QString getLabel(){return QString("");}
    ~CreepCalc();
private:
    unsigned long int iAge;
    bool finished;
    Tile *arrTiles[16][10];
    int Health ;
    QBrush brush ;
    QPointF target ;
    TemplateItem *tTarget ;


};
#endif // CREEPCALC_H
