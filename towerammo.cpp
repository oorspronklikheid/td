#include "towerammo.h"
#include "creep.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "templateitem.h"
TowerAmmo::TowerAmmo(QPointF qpoint): TemplateItem( qpoint)
{
    setPos(qpoint);
    Health = 10;
    AssignId();
    Count = 0 ;
    stepleftover =0;
}
TowerAmmo::TowerAmmo(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem*> *item,Tile *arrTiles[16][10]): TemplateItem( qpoint,newlayout,item,arrTiles)
{
    stepleftover =0;
    setPos(qpoint);
    Health = 30;
    items = item ;
    AssignId();
    //    target = NearestCreep(pos());
    //    target = QPointF(0,0);
    Count = 0 ;
    iTarget = NULL ;
}
int TowerAmmo::getDamage()
{
    return 42 ;
}

QPointF TowerAmmo::NearestCreep(QPointF point)
{
    qreal x1 ,x2 ,y1 ,y2 ,l, s ;
    QPointF dest ;
    bool b = false ;
    x1 = point.x() ;
    y1 = point.y() ;
    s = 10000000;
    for(int i = 0 ; i < items->size();i++)
    {
        if(items->value(i)->getSubType() == lvl1creep)
        {
            //            qDebug() << i ;
            //            qDebug() <<  items[i]->pos();

            x2 = items->value(i)->pos().x();
            y2 = items->value(i)->pos().y();
            l = ( x1 - x2  )*( x1 - x2  ) + ( y1 - y2 )* ( y1 - y2 )  ;
            if(l < s)
            {
                //                qDebug() << true ;
                s = l ;
                dest = items->value(i)->pos();
                b = true ;
            }

        }
    }
    //    qDebug() << "point" << point;
    //    qDebug() << "dest" << dest;
    if (b==false )
        setHealth(0);
    return dest ;
}

QPointF TowerAmmo::gettarget()
{
    return target ;
}

QRectF TowerAmmo::boundingRect() const
{
    qreal penWidth = 1;
    //    return QRectF(-10 - penWidth / 2, -10 - penWidth / 2,
    //                  20 + penWidth, 20 + penWidth);
    return QRectF( -11 - penWidth / 2, -11 - penWidth / 2, 22 , 22) ;
}


void TowerAmmo::settarget(QPointF point)
{
    target = point ;// qDebug() << "void TowerAmmo::settarget(QRectF)";
    //    qDebug() << "traget is : " << target ;
}
void TowerAmmo::settarget(TemplateItem *item)
{
    iTarget = item ;// qDebug() << "void TowerAmmo::settarget(QRectF)";
    //    qDebug() << "traget is : " << target ;
}

void TowerAmmo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                      QWidget *widget)
{
    int length ;
    length =10 ;

    qreal x , y , theta ;
    if(iTarget!=NULL && iTarget->scene() != 0 )
    {
        x = iTarget->pos().x() - pos().x();
        y = iTarget->pos().y() - pos().y();
    }else
    {
//        target = NearestCreep(pos());
        x = target.x() - pos().x() ;
        y = target.y() - pos().y() ;
    }
    theta = qAtan2(x,y) + 3.141;
    theta = -1* theta * 57,295779 ;
//    qDebug() << "theta" << theta;
    painter->rotate(theta);

    QVector<QPointF> points;
    points << QPointF( 0, -length ) << QPointF( -length/1.414, length/1.414 ) << QPointF( length/1.414, length/1.414)  ;
    painter->drawPolygon(points);
    //  ______________________________
    //  |        (x1,y1-y)            |
    //  |                             |
    //  |       (0,0)                 |
    //  |                             |
    //  |(x2-x,y2+y)       (x3+x,y3+y)|
    //  ------------------------------|

}

int TowerAmmo::getHealth()
{
    return Health ;
}
void TowerAmmo::setHealth(int health)
{
    Health = health ;
}
long int TowerAmmo::getID()
{
    return ID;
}
void TowerAmmo::Progress(qreal step)
{
    while(stepleftover >= 50)
    {
        stepleftover = stepleftover - 50 ;
        Count++ ;
    }
//    Count ++ ;
    Count = Count % 8 ;
    if(Count == 0 )
    {
        Health -- ;
        Count++;
    }
        //        target = NearestCreep(pos());

        //use this function to update the creep

    //    qDebug() << "Health" << Health ;
    qreal dx , dy ;
    qreal max = 20*(step/50.0) ;

    //    qDebug() <<"DX" << dx << "dy" << dy;
    qreal dr = max;
    qreal r,x,y,theta;

    if(iTarget!=NULL && iTarget->scene() != 0 )
    {
        x = iTarget->pos().x() - pos().x();
        y = iTarget->pos().y() - pos().y();
    }else
    {

        target = NearestCreep(pos());
//        qDebug() << "getting new target at" <<target;
        x = target.x() - pos().x() ;
        y = target.y() - pos().y() ;

    }
    r = qSqrt(x*x +y*y);
    if(r < dr)
        dr = r ;// t/a = tan theta
    // t = y
    // a = -x
    // -y/x = tan theta
    // tan-1(-y/x) = theta

            theta = qAtan2(x,y);
//    qDebug() << "theta" << theta;

    dx =  dr*qSin(theta);
    dy =  dr* qCos(theta);

    setPos(pos().x() + 0.9*dx,pos().y() +0.9*dy);
    update();


}

TowerAmmo::~TowerAmmo()
{
    //    qDebug() << "smells like something died";
}
