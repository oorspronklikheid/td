#include "creepcalc.h"
#include "QRectF"
#include "QtGui"
#include "QPoint"
#include "QBrush"
#include "templateitem.h"
#include "QVector"
#include "tile.h"
CreepCalc::CreepCalc( QPointF qpoint) : TemplateItem( qpoint)
{
    setPos(qpoint);
    Health = 100 ;
    AssignId();
    qDebug() << "oops";

}
CreepCalc::CreepCalc(QPointF qpoint , Layoutwidget *newlayout, QVector<TemplateItem *> *item, Tile *arrtiles[16][10] ): TemplateItem( qpoint,newlayout,item,arrtiles)
{
    iAge = 0;
    finished =false ;
    setEnabled(false) ;


    for(int i=0; i < 16 ; i++ )
        for(int j=0; j < 10 ; j++ )
        {
            arrTiles[i][j] = arrtiles[i][j] ;
            //            arrTiles[i][j]->setPassable(false);

            qreal dx ,dy ,r ;
        }
    setPos(qpoint);
    Health = 100 ;
    items = item ;
    AssignId();
    for(int i= items->size()-1 ; i >= 0 ; i--)
    {
        if(items->value(i)->getSubType() == creepwaypoint)
        {
            target = items->value(i)->pos();
            //            if(dynamic_cast<CreepWayPoint*>(item->value(i)))
            tTarget = items->value(i);
            //            qDebug() << "found a point" << tTarget;
        }

    }
    //    Progress();
}

long int CreepCalc::getID()
{
    return ID;
}

QRectF CreepCalc::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(-10 - penWidth / 2, -10 - penWidth / 2,
                  20 + penWidth, 26 + penWidth);

}
void CreepCalc::setHealth(int health)
{
    Health = health ;

}

int CreepCalc::getHealth()
{
    return Health ;
}


void CreepCalc::settarget(QPointF)
{

}

void CreepCalc::Progress(qreal step)
{
    if(finished==false)
    {

        qreal x,y,r ;
        for(int i=0; i < 16 ; i++ )
            for(int j=0; j < 10 ; j++ )
            {
                x = arrTiles[i][j]->pos().x() - pos().x();
                x = x*x ;
                y = arrTiles[i][j]->pos().y() - pos().y();
                y = y*y ;
                r = qSqrt(x+y);

                //            if(collidesWithItem(arrTiles[i][j])==true)
                if(r<50)
                {
                    if(arrTiles[i][j]->getPassable() == true)
                    {
                        arrTiles[i][j]->setBuildable(false);
                        iAge++;
                        arrTiles[i][j]->setAge(iAge);
//                        qDebug() << "i have a collision at age" << iAge <<"I"<<i << "J"<<j <<"pos()" << pos();
                    }

                }
            }


        //use this function to update the creep
        QPointF point;
        int j = -1 , k=-1;

        for(int i=0 ; i < items->size();i++) // find the targets position in vector
        {
            if(items->value(i)->getSubType() == creepwaypoint)
            {
                if(target == items->value(i)->pos() )
                    j = i ; //signifies position of the waypoint in items
            }

        }
        for(int i=0 ; i < items->size();i++) // find this items position in vector
        {
            if(items->value(i)->getSubType() == creepcalc)
            {
                if(pos() == items->value(i)->pos())
                    k = i ; //signifies position of the creep in items
            }

        }


        if(tTarget->pos() ==pos())
        {
            int d = -1 ;
            for(int i=0 ; i < items->size();i++)
            {
                if(tTarget == items->value(i))
                {
                    //                qDebug() << "Found myself :P";
                    d = i ;
                }

            }
            //        qDebug() << "looking for next waypoint";
            bool didfind=false;
            for(int i = items->size()-1 ; i>d  ;i--) //we want the next waypoint
            {
                //            qDebug() << "pew" << i;
                if(items->value(i)->getSubType() == creepwaypoint)
                {

                    tTarget = items->value(i);
                    didfind = true;
                }
            }

        }

        qreal dx , dy ;
        int max = 10 ;
        //    dx = target.x() - pos().x() ;
        //    dy = target.y() - pos().y() ;
        dx = tTarget->pos().x() - pos().x() ;
        dy = tTarget->pos().y() - pos().y() ;

        if (dx > max) dx = max ;
        if (dy > max) dy = max ;
        if (dx < -max) dx = -max ;
        if (dy < -max) dy = -max ;

        setPos(pos().x() + dx,pos().y() +dy);
        // setPos(pos().x()+4,pos().y() + 4);


        update();

        if(dx != 0 || dy != 0)
        {
            //        qDebug() << "progressiong";
            Progress(0);
        }else finished = true;
    }
}

void CreepCalc::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    painter->setPen(QColor(0,0,0,0));
    painter->drawRoundedRect(-10, -10, 20, 20, 5, 5);


}
CreepCalc::~CreepCalc()
{
    //    qDebug() << "smells like something died";
}
