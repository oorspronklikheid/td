#ifndef MYGRAPHICSSCENE_H
#define MYGRAPHICSSCENE_H

#include "QGraphicsScene"
#include "templateitem.h"
#include "guibutton.h"
class MyGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit MyGraphicsScene(QObject *parent = 0);
    explicit MyGraphicsScene( QVector<TemplateItem *> *item   , Layoutwidget *newlayout ,QWidget *parent = 0,Tile *arrtiles[16][10] =NULL , QRectF size=QRectF(0,0,900,650));
    Tile *arrTiles[16][10]  ;
    QVector<TemplateItem*> *items ;
    Layoutwidget *layout ;
    
signals:
    void spawn(TemplateItem* item);
    void deleteItem(TemplateItem* item);
    void notify(gmmode mode);
    void mousemoved(QPointF);
    void setPlay(bool state);
    
public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void keyPressEvent ( QKeyEvent * keyEvent ) ;
    void modeChange(gmmode mode);

private:
    gmmode Mode;
};

#endif // MYGRAPHICSSCENE_H
