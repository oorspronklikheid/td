#ifndef TILE_H
#define TILE_H
#include "templateitem.h"
#include "QRectF"
#include "QGraphicsObject"
#include "QString"
#include "QPoint"
#include "QBrush"
//#include "QVector"
//class TemplateItem;
class Tile :   public  QGraphicsObject
{
    Q_OBJECT
public:
    Tile(QPointF qpoint,Layoutwidget *newlayout);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    void SetProgressID(unsigned int ticks);
    void setBuildable(bool pass);
    void Subscribed(TemplateItem *item=NULL);
    void unSubscribed(TemplateItem *item);
    bool getPassable(void){return Buildable;}
    void setAge(int age);
    int getAge(void){return iAge;}
    QVector<TemplateItem*> creepsAtTile ;
    QVector<TemplateItem*> itemsAtTile ;

    void setImgPath(QImage *image);
    void setImgnoPath(QImage *image);
    void resetState();

    void setTowerPresent(bool newstate){TowerPresent = newstate;}
    bool getTowerPresent(){return TowerPresent ;}
private:
    Layoutwidget *layout;
    QImage *path;
    QImage *nopath;
    bool bpath;
    bool bnopath;
//    itemsAtTile(QVector<TemplateItem *>,(1));
    bool Buildable;
    bool TowerPresent; //true if there is an tower
    unsigned int iAge;

};

#endif // TILE_H
