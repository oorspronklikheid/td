#ifndef TOWERAMMO2_H
#define TOWERAMMO2_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
//enum typeItem { creep , tower , towerammo , null  };

class TowerAmmo2 : public TemplateItem
{    Q_OBJECT
 public:
     TowerAmmo2(QPointF qpoint);
     TowerAmmo2(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrtiles[16][10]);
      Tile *arrTiles[16][10]  ;
     QRectF boundingRect() const;
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
     void Progress(qreal step);
     void settarget(QPointF);

     typesubItem getSubType(){return towerammo2;}
     typemainItem getMainType(){return ammo;}

     int getHealth();
     void setHealth(int health);
     long int getID();
     QPointF gettarget();
     void setImage(QImage *newimage,int rank){}
     int getDamage();
     int getWorth(){}
     virtual void setModifier(typemodifier newmodifier){}
      static QString getLabel(){return QString("");}
          ~TowerAmmo2();
 private:
//      QPointF NearestCreep(QPointF point);
      QPointF target ;

//      int target ;
      qreal stepleftover;
      int Health ;
      int Count;
//       QVector<TemplateItem*>  items ;
};

#endif // TOWERAMMO2_H

