#ifndef TOWERAMMO_H
#define TOWERAMMO_H

#include "QRectF"
#include "QGraphicsItem"
#include "QPoint"
#include "templateitem.h"
//enum typeItem { creep , tower , towerammo , null  };

class TowerAmmo : public TemplateItem
{    Q_OBJECT
 public:
     TowerAmmo(QPointF qpoint);
     TowerAmmo(QPointF qpoint , Layoutwidget *newlayout , QVector<TemplateItem *> *item, Tile *arrTiles[16][10]);
     QRectF boundingRect() const;
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
     void Progress(qreal step);
     void settarget(QPointF);
     void settarget(TemplateItem *item);

      typesubItem getSubType(){return towerammo;}
      typemainItem getMainType(){return ammo;}

     int getHealth();
     void setHealth(int health);
      int getWorth(){}

     long int getID();
     QPointF gettarget();
     void setImage(QImage *newimage,int rank){}
     int getDamage();
      virtual void setModifier(typemodifier newmodifier){}
      static QString getLabel(){return QString("");}
          ~TowerAmmo();

 private:
      QPointF NearestCreep(QPointF point);
      QPointF target ;
       TemplateItem *iTarget;
       qreal stepleftover;
       int Count;
//      int target ;
       int Health ;
//       QVector<TemplateItem*>  items ;
};

#endif // TOWERAMMO_H

