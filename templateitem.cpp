#include "templateitem.h"
#include "QRectF"
#include "QtGui"
#include "QPointF"

TemplateItem::TemplateItem(QPointF qpoint)
{
    setPos(qpoint);
}

TemplateItem::TemplateItem(QPointF qpoint,Layoutwidget *newlayout, QVector<TemplateItem*> *varitem = NULL, Tile *arrTiles[16][10] = NULL)
{
    layout = newlayout;
    items = varitem;
    setPos(qpoint);
//    qDebug()<<"Hi , I am" <<getType() ;
   // qDebug()<<"TemplateItem::TemplateItem(QPointF qpoint,QVector<TemplateItem*> *varitem = NULL)";
}

QVector<TemplateItem*>* TemplateItem::GetItem()
{
return items ;
}

void TemplateItem::AssignId()
{
    if(items->size()>1)
    {
        ID = items->value(items->size()-1)->getID()+1 ;
    }
    else
        ID = 0 ;
//    qDebug() << "My id is:" << ID ;
}
TemplateItem::~TemplateItem()
{

}
