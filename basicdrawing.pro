HEADERS       = \
                window.h \
    gamearea.h \
    creep.h \
    tower.h \
    towerammo.h \
    templateitem.h \
    creepspawner.h \
    creepwaypoint.h \
    mygraphicsview.h \
    guibutton.h \
    hoverlabel.h \
    staticlabel.h \
    tower2.h \
    towerammo2.h \
    tile.h \
    preview.h \
    creepcalc.h \
    timescript.h \
    levelbackground.h \
    menuitem.h \
    menu.h \
    mygraphicsscene.h \
    creepboss.h \
    lvl2creep.h \
    lvl2creepspawner.h \
    lvl2creepboss.h \
    coldtower.h \
    coldammo.h \
    layoutwidget.h \
    acidtower.h \
    acidammo.h
SOURCES       = main.cpp \
                window.cpp \
    gamearea.cpp \
    creep.cpp \
    tower.cpp \
    towerammo.cpp \
    templateitem.cpp \
    creepspawner.cpp \
    creepwaypoint.cpp \
    mygraphicsview.cpp \
    guibutton.cpp \
    hoverlabel.cpp \
    staticlabel.cpp \
    tower2.cpp \
    towerammo2.cpp \
    tile.cpp \
    preview.cpp \
    creepcalc.cpp \
    timescript.cpp \
    levelbackground.cpp \
    menuitem.cpp \
    menu.cpp \
    mygraphicsscene.cpp \
    creepboss.cpp \
    lvl2creep.cpp \
    lvl2creepspawner.cpp \
    lvl2creepboss.cpp \
    coldtower.cpp \
    coldammo.cpp \
    layoutwidget.cpp \
    acidtower.cpp \
    acidammo.cpp
RESOURCES     = basicdrawing.qrc

# install
target.path = $$[QT_INSTALL_EXAMPLES]/painting/basicdrawing
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS basicdrawing.pro images
sources.path = $$[QT_INSTALL_EXAMPLES]/painting/basicdrawing
INSTALLS += target sources

symbian {
    TARGET.UID3 = 0xA000A649
    include($$QT_SOURCE_TREE/examples/symbianpkgrules.pri)
}
maemo5: include($$QT_SOURCE_TREE/examples/maemo5pkgrules.pri)

simulator: warning(This example might not fully work on Simulator platform)

OTHER_FILES += \
    TODO.txt \
    leveldata.txt \
    levels/level1.txt \
    levels/level2.txt \
    graphics/waypoint-4.png \
    graphics/waypoint-3.png \
    graphics/waypoint-2.png \
    graphics/waypoint.png \
    graphics/Tower2-new-preview.png \
    graphics/Tower2-new.png \
    graphics/Tower2-4.png \
    graphics/Tower2-3.png \
    graphics/Tower2-2.png \
    graphics/Tower2.png \
    graphics/tower1-weakest.png \
    graphics/tower1-strongest.png \
    graphics/tower1-preview.png \
    graphics/tower1-nearest.png \
    graphics/tower1-eldest.png \
    graphics/spawner-2.png \
    graphics/spawner.png \
    graphics/endpoint-2.png \
    graphics/endpoint.png \
    graphics/level1.png \
    levels/level3.txt \
    graphics/stonetile.png \
    graphics/path.png \
    graphics/level2.png \
    graphics/grasstile.png \
    graphics/background.png \
    graphics/winscreen.png \
    graphics/losescreen.png \
    graphics/backgroundnew.png \
    graphics/lvl2creep.png
